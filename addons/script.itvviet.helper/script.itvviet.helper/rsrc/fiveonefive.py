#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc, xbmcgui
import hashlib, os, platform, re, requests, shutil, subprocess, zipfile

# ------------------------------------------------------------------------------------------------------------------------------------ #

def UPDATETO15():
	PREPARE()

def PREPARE(): # 1 of 7/9 ###############################################################################
	retry = False
	prompt = True
	# Remove existing KODI folders if currently using SPMC
	if app_id == 'com.semperpax.spmc':
		installed_apps = INVENTORY()
		if 'org.xbmc.kodi' in installed_apps or os.path.isdir('/sdcard/Android/data/org.xbmc.kodi'):
			prompt = xbmcgui.Dialog().yesno('[1/'+str(steps)+'] KODI Exists','Remove KODI to continue?','Xóa bỏ KODI để tiếp tục?')
			if prompt:
				pDialog = POPUP('[ Step 1 of '+str(steps)+' ]','Removing KODI / Xóa bỏ KODI...','',0,False)
				try:
					subprocess.call([cmd_+'/system/bin/rm -rf /sdcard/Android/data/org.xbmc.kodi'+_cmd], executable='/system/bin/sh', shell=True)
				except:
					retry = xbmcgui.Dialog().yesno('[1/'+str(steps)+'] KODI Deletion Failed','Try again / Thử lại?')
				if 'org.xbmc.kodi' in installed_apps:
					pDialog = POPUP('[ Step 1 of '+str(steps)+' ]','Uninstalling KODI / Xóa bỏ KODI...','',0,False)
					UNINSTALL('org.xbmc.kodi',1)
				if retry:
					PREPARE()
	if prompt == True:
		pDialog = POPUP('[ Step 1 of '+str(steps)+' ]','Starting / Bắt đầu...','',0,False)
		CLEAR(folders1,files1) # Free up space
		try:
			availdata = subprocess.Popen(["/system/bin/df /data  | tail -1 | awk '{print $4}'"], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n')
			if 'not found' in availdata: raise ValueError('Cannot calculate available space!')
		except:
			disk = re.compile('\/data[ \t]+(.+?)[ \t]+(.+?)[ \t]+(.+?)[ \t]+(.+)').findall(subprocess.Popen(["/system/bin/df /data"], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n'))
			availdata = disk[0][2]
		finally:
			if 'G' in availdata: availdata = float(availdata.replace('G',''))*1000
			elif 'M' in availdata: availdata = float(availdata.replace('M',''))
			else: availdata = 0
		if availdata < reqMB:
			xbmcgui.Dialog().ok('[1/'+str(steps)+'] Not Enough Space','Only '+str(availdata).replace('.0','')+' MB available',str(reqMB)+' MB is required')
		else:
			DOWNLOAD()

def DOWNLOAD(): # 2 of 7/9 ###############################################################################
	# Checks if zip file already downloaded
	if os.path.isfile(tmp+zip):
		CHECK(False)
	else:
		CLEAN_TMP()
		MAKE_TMP()
		# Start downloading
		retry = False
		try:
			with open(tmp+zip, "wb") as f:
				pDialog = POPUP('[ Step 2 of '+str(steps)+' ]','Downloading / Đang tải...','',0,True)
				r = requests.get(www+zip, stream=True)
				l = r.headers.get('content-length')
				if not l is None:
					dl = 0
					l = int(l)
					for data in r.iter_content(chunk_size=1024):
						dl += len(data)
						f.write(data)
						done = int(100 * dl / l)
						pDialog.update(done,'','')
						if pDialog.iscanceled():
							break
				del r
			if done < 100: raise ValueError('Download did not finish!')
		except:
			retry = xbmcgui.Dialog().yesno('[2/'+str(steps)+'] Download Failed','Try again / Thử lại?')
		else:
			CHECK(True)
		if retry:
			DOWNLOAD()

def CHECK(prompt): # 3 of 7/9 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 3 of '+str(steps)+' ]','Checking / Kiểm tra...','',0,False)
	try:
		z = zipfile.ZipFile(tmp+zip, 'r')
		z.testzip()
	except:
		if prompt:
			retry = xbmcgui.Dialog().yesno('[3/'+str(steps)+'] File Check Failed','Try again / Thử lại?')
		else:
			retry = True
	else:
		EXTRACT(z)
	if retry:
		CLEAN_TMP()
		DOWNLOAD()

def EXTRACT(z): # 4 of 7/8 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 4 of '+str(steps)+' ]','Extracting / Giải nén...','',0,False)
	try:
		z.extractall(tmp)
		z.close()
	except:
		retry = xbmcgui.Dialog().yesno('[4/'+str(steps)+'] File Extraction Failed','Try again / Thử lại?')
	else:
		shutil.move(tmp+apk,temp+apk) # Move APK file to temporary for staging
		UPDATE_SYSTEM()
	if retry:
		DOWNLOAD()

def UPDATE_SYSTEM(): # 5 of 7/9 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 5 of '+str(steps)+' ]','Copying / Sao chép...','',0,False)
	try: # Open system write access
		subprocess.call([cmd_+'/system/bin/mount -o remount,rw /system'+_cmd], executable='/system/bin/sh', shell=True)
	except:
		xbmcgui.Dialog().ok('[5/'+str(steps)+'] No Root Access','Cannot write files')
	else:
		pDialog = POPUP('[ Step 5 of '+str(steps)+' ]','Updating app backup / Cập nhật app backup...','',0,False)
		UPDATE_SYSTEM_APP()

		pDialog = POPUP('[ Step 5 of '+str(steps)+' ]','Updating backup files / Cập nhật backup...','',0,False)
		UPDATE_SYSTEM_FILES()

		if app_id == 'com.semperpax.spmc':
			pDialog = POPUP('[ Step 5 of '+str(steps)+' ]','Updating HOME menu / Cập nhật HOME menu...','',0,False)
			UPDATE_SYSTEM_HOME()

		# Close system write access ###############################################################################
		subprocess.call([cmd_+'/system/bin/mount -r -o remount /system'+_cmd], executable='/system/bin/sh', shell=True)

		if app_id == 'com.semperpax.spmc':
			COPY_SPMC()
		elif app_id == 'org.xbmc.kodi':
			COPY_UPDATES()

def UPDATE_SYSTEM_APP(): # 5a of 7/9 ###############################################################################
	retry = False
	try: # Delete old app from system
		if os.path.isfile('/system/preinstall/itvviet-spmc14.2_20150507.apk'): subprocess.call([cmd_+'/system/bin/rm -f /system/preinstall/itvviet-spmc14.2_20150507.apk'+_cmd], executable='/system/bin/sh', shell=True)
		if os.path.isfile('/system/app/itvviet-spmc14.2_20150507.nm'): subprocess.call([cmd_+'/system/bin/rm -f /system/app/itvviet-spmc14.2_20150507.nm'+_cmd], executable='/system/bin/sh', shell=True)
		if os.path.isfile('/system/preinstall/itvviet-1k_kodi14.2_20150717.apk'): subprocess.call([cmd_+'/system/bin/rm -f /system/preinstall/itvviet-1k_kodi14.2_20150717.apk'+_cmd], executable='/system/bin/sh', shell=True)
		if os.path.isfile('/system/preinstall/itvviet-4k_kodi14.2_20150717.apk'): subprocess.call([cmd_+'/system/bin/rm -f /system/preinstall/itvviet-4k_kodi14.2_20150717.apk'+_cmd], executable='/system/bin/sh', shell=True)
	except:
		retry = xbmcgui.Dialog().yesno('[5/'+str(steps)+'] App Deletion Failed','Try again / Thử lại?')
	else:
		try: # Copy over new app to system
			if str(hashlib.sha512(model).hexdigest()) == rkchip4:
				subprocess.call([cmd_+'/system/bin/cp -f '+temp+apk+' /system/app/'+apk.replace('.apk','.nm')+_cmd], executable='/system/bin/sh', shell=True)
				subprocess.call([cmd_+'/system/bin/chown root:root /system/app/'+apk.replace('.apk','.nm')+_cmd], executable='/system/bin/sh', shell=True)
				subprocess.call([cmd_+'/system/bin/chmod 644 /system/app/'+apk.replace('.apk','.nm')+_cmd], executable='/system/bin/sh', shell=True)
			else:
				subprocess.call([cmd_+'/system/bin/cp -f '+temp+apk+' /system/preinstall/'+_cmd], executable='/system/bin/sh', shell=True)
				subprocess.call([cmd_+'/system/bin/chown root:root /system/preinstall/'+apk+_cmd], executable='/system/bin/sh', shell=True)
				subprocess.call([cmd_+'/system/bin/chmod 644 /system/preinstall/'+apk+_cmd], executable='/system/bin/sh', shell=True)
				
		except:
			retry = xbmcgui.Dialog().yesno('[5/'+str(steps)+'] App Copy Failed','Try again / Thử lại?')
	if retry:
		UPDATE_SYSTEM_APP()

def UPDATE_SYSTEM_FILES(): # 5b of 7/9 ###############################################################################
	retry = False
	try: # Update system .zip backup and preinstall.sh script
		if os.path.isfile('/system/usr/kodi.zip'):
			#Standardize .zip file name
			subprocess.call([cmd_+'/system/bin/mv /system/usr/kodi.zip /system/usr/heidi.zip '+_cmd], executable='/system/bin/sh', shell=True)
		if os.path.isfile('/system/usr/heidi.zip'):
			#Copies .zip updates and sets permissions
			subprocess.call([cmd_+'/system/bin/cp -f '+tmp+updz+' /system/usr/'+updz+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chown root:root /system/usr/'+updz+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chmod 644 /system/usr/'+updz+_cmd], executable='/system/bin/sh', shell=True)
			#Updates advancedsettings.xml and sets permissions
			subprocess.call([cmd_+'/system/bin/cp -f '+tmp+'advancedsettings.xml /system/usr/advancedsettings.xml'+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chown root:root /system/usr/advancedsettings.xml'+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chmod 644 /system/usr/advancedsettings.xml'+_cmd], executable='/system/bin/sh', shell=True)
			#Updates preinstall.sh script and sets permissions
			subprocess.call([cmd_+'/system/bin/cp -f '+tmp+'preinstall.sh /system/bin/preinstall.sh'+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chown root:root /system/bin/preinstall.sh'+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chmod 755 /system/bin/preinstall.sh'+_cmd], executable='/system/bin/sh', shell=True)
		else:
			if app_id == 'com.semperpax.spmc':
				subprocess.call([cmd_+'/system/bin/mv /system/extMedia/Android/data/com.semperpax.spmc /system/extMedia/Android/data/org.xbmc.kodi'+_cmd], executable='/system/bin/sh', shell=True)
				subprocess.call([cmd_+'/system/bin/mv /system/extMedia/Android/data/org.xbmc.kodi/files/.spmc /system/extMedia/Android/data/org.xbmc.kodi/files/.kodi'+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/cp -Rf '+tmp+upd+'. /system/extMedia/Android/data/org.xbmc.kodi/files/.kodi/'+_cmd], executable='/system/bin/sh', shell=True)
#SLOW		PERM('/system/extMedia/Android/data/org.xbmc.kodi/files/.kodi/')

	except:
		retry = xbmcgui.Dialog().yesno('[5/'+str(steps)+'] Backup Update Failed','Try again / Thử lại?')
	if retry:
		UPDATE_SYSTEM_FILES()

def UPDATE_SYSTEM_HOME(): # 5c of 7/9 ###############################################################################
	retry = False
	try: # Update homelauncher order if SPMC
		subprocess.call([cmd_+'/system/bin/cp -f /system/'+launcher+'.json '+tmp+_cmd], executable='/system/bin/sh', shell=True)
		with open(tmp+launcher+'.json', 'r+') as f:
			file = f.read()
			f.seek(0)
			f.write(file.replace('com.semperpax.spmc','org.xbmc.kodi'))
			f.truncate()
		f.close()
		subprocess.call([cmd_+'/system/bin/cp -f '+tmp+launcher+'.json /system/'+_cmd], executable='/system/bin/sh', shell=True)
		subprocess.call([cmd_+'/system/bin/chown root:root /system/'+launcher+'.json'+_cmd], executable='/system/bin/sh', shell=True)
		subprocess.call([cmd_+'/system/bin/chmod 644 /system/'+launcher+'.json'+_cmd], executable='/system/bin/sh', shell=True)
	except:
		retry = xbmcgui.Dialog().yesno('[5/'+str(steps)+'] Home Menu Update Failed','Try again / Thử lại?')
	if retry:
		UPDATE_SYSTEM()

def COPY_SPMC(): # 6 of 9 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 6 of 9 ]','Copying files / Sao chép tại liệu...','',0,False)
	success = COPYR(home,home_kodi,homeN)
	if success:
		COPY_UPDATES()
	else:
		retry = xbmcgui.Dialog().yesno('[6/9] Files Copy Failed','Try again / Thử lại?')
	if retry:
		COPY_SPMC()

def COPY_UPDATES(): # 6/7 of 7/9 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step '+str(6+max(0,steps-8))+' of '+str(steps)+' ]','Copying additional / Sao chép thêm...','',0,False)
	success = COPYR(tmp+upd,home_kodi,updN)
	if success:
		pDialog = POPUP('[ Step '+str(6+max(0,steps-8))+' of '+str(steps)+' ]','Clearing cache...','',0,False)
		CLEAR(folders2,files2) # Clears cache
		CLEAN_TMP() # Cleans tmp
		INSTALL_KODI()
	else:
		retry = xbmcgui.Dialog().yesno('['+str(6+max(0,steps-8))+'/'+str(steps)+'] Files Copy Failed','Try again / Thử lại?')
	if retry:
		COPY_UPDATES()

def INSTALL_KODI(): # 7/8 of 7/9 ###############################################################################
	retry = False
	# Close Home Launcher
	try:
		subprocess.call([cmd_+'/system/bin/am force-stop '+launcher+_cmd], executable='/system/bin/sh', shell=True)
	except:
		pass
	# DIRECT ROOTED INSTALL
	pDialog = POPUP('[ Step '+str(7+max(0,steps-8))+' of '+str(steps)+' ]','Updating / Đang cập nhật...','',0,False)
	try:
		install_result = subprocess.Popen([cmd_+'/system/bin/pm install -r -d '+temp+apk+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n').splitlines()
		##### KODI END #####
		if not install_result[1]:
			raise ValueError('App was not installed!')
	except:
		if str(hashlib.sha512(model).hexdigest()) == rkchip1:
			# USER PROMPTED INSTALL FOR LEGACY
			xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.packageinstaller","android.intent.action.INSTALL_PACKAGE","application/vnd.android.package-archive","file:'+temp+apk+'")')
			xbmc.sleep(2000)
			retry = xbmcgui.Dialog().yesno('Confirmation','Operation completed','Hoạt động hoàn thành','','OK','Retry')
			if retry:
				INSTALL_KODI()
			else:
				os.remove(temp+apk) # Deletes used APK file
				pDialog = POPUP('[ Step 8 of 9 ]','Almost done / Gần xong...','',0,False)
				UNINSTALL(app_id,9) ##### SPMC END #####
		else:
			retry = xbmcgui.Dialog().yesno('['+str(6+max(0,steps-8))+'/'+str(steps)+'] Updating Failed','Try again / Thử lại?')
	else:
		# Continues with SPMC copy
		if install_result[1] == 'Success':
			os.remove(temp+apk) # Deletes used APK file
			pDialog = POPUP('[ Step 8 of 9 ]','Almost done / Gần xong...','',0,False)
			UNINSTALL(app_id,9) ##### SPMC END #####
		else:
			xbmcgui.Dialog().ok('Error',install_result[1])
	if retry:
		INSTALL_KODI()

def UNINSTALL(package,step): # 1/9 of 9 ###############################################################################
	retry = False
	# DIRECT ROOTED UNINSTALL
	try:
		uninstall_result = subprocess.Popen([cmd_+'/system/bin/pm uninstall '+package+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n')
		if uninstall_result == 'Success' or uninstall_result == True:
			pass
		else:
			raise ValueError('App was not removed!')
	except:
		if str(hashlib.sha512(model).hexdigest()) == rkchip1:
			#USER PROMPTED UNINSTALL
			#ADB SHELL: am start -a android.intent.action.DELETE -n com.android.packageinstaller/.UninstallerActivity package:somepackage.apk
			xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.packageinstaller","android.intent.action.DELETE","","package:'+package+'")')
			xbmc.sleep(2000)
			retry = xbmcgui.Dialog().yesno('Confirmation','Operation completed','Hoạt động hoàn thành','','OK','Retry')
			if retry:
				UNINSTALL(package,step)
		else:
			retry = xbmcgui.Dialog().yesno('['+step+'/'+str(steps)+'] App Not Removed','Try again / Thử lại?')
	if retry:
		UNINSTALL(package,step)

# ------------------------------------------------------------------------------------------------------------------------------------ #

def APP_ID(): ###############################################################################
	xbmcfolder=xbmc.translatePath('special://home/').split('/')
	found = False
	i = 0
	for folder in xbmcfolder:
		if folder.count('.') >= 2 :
			found = True
			break
		else:
			i+=1
	if found == True:
		app_id = xbmcfolder[i]
	return app_id

def INVENTORY(): ###############################################################################
	try:
		installed_apps = subprocess.Popen([cmd_+'/system/bin/pm list packages'+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n').splitlines()
	except:
		installed_apps = []
	else:
		for i in range(len(installed_apps)):
			if platform_release == '3.10.0' or platform_release == '3.0.36+':
				installed_apps[i] = installed_apps[i].partition(':')[2]
			else:
				installed_apps[i] = installed_apps[i].partition('=')[2]
	return installed_apps

def POPUP(line1,line2,line3,progress,button): ###############################################################################
	pDialog.update(progress,line1,line2,line3)
	pDialog_Window = xbmcgui.Window(10101)
	pDialog_cancelButton = pDialog_Window.getControl(10)
	pDialog_cancelButton.setEnabled(button)
	return pDialog

pDialog = xbmcgui.DialogProgress()
pDialog.create('Please Wait','','','')

def CLEAN_TMP(): ###############################################################################
#	subprocess.call([cmd_+'/system/bin/rm -rf '+tmp+_cmd], executable='/system/bin/sh', shell=True)
	try:
		shutil.rmtree(tmp.rstrip('/'))
	except:
		pass

def MAKE_TMP(): ###############################################################################
	if not os.path.isdir(tmp):
		try:
			os.mkdir(tmp)
			subprocess.call([cmd_+'/system/bin/chown system:sdcard_rw '+tmp+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chmod 755 '+tmp+_cmd], executable='/system/bin/sh', shell=True)
		except:
			xbmcgui.Dialog().ok('Folder Creation Failed','Cannot create Temp folder')

def COPYR(root_src_dir,root_dst_dir,count): ###############################################################################
	file = 0
	pDialog = POPUP('','','',0,False)
	if not os.path.isdir(root_dst_dir):
		subprocess.call([cmd_+'/system/bin/mkdir -p '+root_dst_dir+_cmd], executable='/system/bin/sh', shell=True)
	try:
		p = subprocess.Popen([cmd_+'/system/bin/cp -Rfv '+root_src_dir+'. '+root_dst_dir+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		while True:
			line = p.stdout.readline()
			if not line:
				break
			file = file+1
			done = int(file*100/count)
			pDialog.update(done,'','','')
	except:
		return False
	else:
		return True

#def ZIPMG(zip1,zip2): ###############################################################################
#	import contextlib
#	zips = [zip1,zip2]
#	with contextlib.closing(zipfile.ZipFile(zips[0], 'a')) as z1:
#		for fname in zips[1:]:
#			zf = zipfile.ZipFile(fname, 'r')
#			for n in zf.namelist():
#				z1.writestr(n, zf.open(n).read())

#def PERM(root_dst_dir): ###############################################################################
#	root_dst_dir = root_dst_dir.rstrip('/')
#	subprocess.call(['find '+root_dst_dir+' -type d -exec su -c \'/system/bin/chown root:root "{}"\' \; -exec su -c \'/system/bin/chmod 755 "{}"\' \;'], executable='/system/bin/sh', shell=True)
#	subprocess.call(['find '+root_dst_dir+' -type f -exec su -c \'/system/bin/chown root:root "{}"\' \; -exec su -c \'/system/bin/chmod 644 "{}"\' \;'], executable='/system/bin/sh', shell=True)

def CLEAR(folders,files): ###############################################################################
#Folders
	for i in range(len(folders)):
		if os.path.isdir(folders[i]):
			try:
				shutil.rmtree(folders[i])
			except:
				pass
#Files
	for i in range(len(files)):
		if os.path.isfile(files[i]):
			try:
				os.remove(files[i])
			except:
				pass

# ------------------------------------------------------------------------------------------------------------------------------------ #

www       = 'https://s3.amazonaws.com/itvviet/'
zip       = 'vk152_20160214.zip' # Zip package
apk       = 'vk152_20151111.apk' # Updated APK
updz      = 'vk152.zip'          # Misc. files to update
upd       = 'vk152/'             # Uncompressed version of misc. files
updN      = 150                  # Count of misc. files
reqMB     = 500                  # Required free space

sdcard    = '/sdcard/'
app_id    = APP_ID() # Use fixed, pre-defined /sdcard paths instead of variable /storage/emulated/0/... -vs- /storage/emulated/legacy/...
if app_id == 'com.semperpax.spmc': home = '/sdcard/Android/data/com.semperpax.spmc/files/.spmc/'; steps = 9
if app_id == 'org.xbmc.kodi': home  = '/sdcard/Android/data/org.xbmc.kodi/files/.kodi/'; steps = 7
home_kodi = '/sdcard/Android/data/org.xbmc.kodi/files/.kodi/'
app_cache = os.path.join('/data/data/', app_id, '/')
tmp       = os.path.join(home, 'tmp/')
temp      = os.path.join(home, 'temp/')
homeN     = 10000                # Count of all files to copy
launcher  = 'com.itv.homelauncher'

rkchip1              = '1d8c4ece2520039f5f0928ba0d8b8f3f01dfc426ec2db07e57fe36992a3d4a18dcdd13cce4659bddddd34410a530628752f69a006b58ff3215c7e4576f41dc75'
rkchip4              = '83db8a25715370079a0c12fc3ff2863e0a572e5457298ce5b300aec32fb43704bbdec6247be051a27aeb637aa57541fd23b4ce6b47c5a950930832120d0d7575'
try: model           = subprocess.Popen(['/system/bin/getprop', 'ro.product.model'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
except: model        = '...'
platform_release     = platform.release().rstrip('\n')

# Parse *NIX commands for compatibility
if platform_release == '3.0.36+':
	cmd_ = 'exec '; _cmd = ''
else:
	cmd_ = 'exec su -c "'; _cmd = '"'

# Pre-ops clear paths
folders1 = [
	os.path.join(home, 'addons/packages'),
	os.path.join(home, 'userdata/Thumbnails'),
#	os.path.join(home, 'userdata/Database/CDDB'),
#	os.path.join(home, 'cache'),
#	os.path.join(home, 'temp'),
#	os.path.join(app_cache, 'cache'),
	os.path.join(sdcard, 'Download/APKs'),
	os.path.join(sdcard, 'show_box'),
	os.path.join(sdcard, 'SingerKaraoke')
]
files1 = [
#	os.path.join(home, 'userdata/Database/Textures13.db'),
#	os.path.join(home, 'userdata/Database/Addons16.db'),
#	os.path.join(home, 'userdata/Database/Epg8.db'),
#	os.path.join(home, 'userdata/Database/MyMusic48.db'),
#	os.path.join(home, 'userdata/Database/MyVideos90.db'),
#	os.path.join(home, 'userdata/Database/TV26.db')
]
# Post-ops clear paths
folders2 = [
	os.path.join(home_kodi, 'addons/packages'),
	os.path.join(home_kodi, 'userdata/Thumbnails'),
	os.path.join(home_kodi, 'userdata/Database/CDDB'),
	os.path.join(home_kodi, 'cache'),
#	os.path.join(home_kodi, 'temp'),
	os.path.join(app_cache, 'cache'),
	os.path.join(sdcard, 'Download/APKs'),
	os.path.join(sdcard, 'show_box'),
	os.path.join(sdcard, 'SingerKaraoke')
]
files2 = [
	os.path.join(home_kodi, 'userdata/Database/Textures13.db'),
	os.path.join(home_kodi, 'userdata/Database/Addons16.db'),
	os.path.join(home_kodi, 'userdata/Database/Epg8.db'),
	os.path.join(home_kodi, 'userdata/Database/MyMusic48.db'),
	os.path.join(home_kodi, 'userdata/Database/MyVideos90.db'),
	os.path.join(home_kodi, 'userdata/Database/TV26.db')
]