#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc, xbmcgui
import hashlib, os, platform, re, requests, shutil, subprocess, zipfile

# ------------------------------------------------------------------------------------------------------------------------------------ #

def UPDATETO15():
	PREPARE()

def PREPARE(): # 1 of 6 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 1 of '+str(steps)+' ]','Starting / Bắt đầu...','',0,False)
	CLEAR(folders1,files1) # Free up space
	try:
		availdata = subprocess.Popen(["/system/bin/df /data  | tail -1 | awk '{print $4}'"], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n')
		if 'not found' in availdata: raise ValueError('Cannot calculate available space!')
	except:
		disk = re.compile('\/data[ \t]+(.+?)[ \t]+(.+?)[ \t]+(.+?)[ \t]+(.+)').findall(subprocess.Popen(["/system/bin/df /data"], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n'))
		availdata = disk[0][2]
	finally:
		if 'G' in availdata: availdata = float(availdata.replace('G',''))*1000
		elif 'M' in availdata: availdata = float(availdata.replace('M',''))
		else: availdata = 0
	if availdata < reqMB:
		xbmcgui.Dialog().ok('[1/'+str(steps)+'] Not Enough Space','Only '+str(availdata).replace('.0','')+' MB available',str(reqMB)+' MB is required')
	else:
		DOWNLOAD()

def DOWNLOAD(): # 2 of 6 ###############################################################################
	# Checks if zip file already downloaded
	if os.path.isfile(tmp+zip):
		CHECK(False)
	else:
		CLEAN_TMP()
		MAKE_TMP()
		# Start downloading
		retry = False
		try:
			with open(tmp+zip, "wb") as f:
				pDialog = POPUP('[ Step 2 of '+str(steps)+' ]','Downloading / Đang tải...','',0,True)
				r = requests.get(www+zip, stream=True)
				l = r.headers.get('content-length')
				if not l is None:
					dl = 0
					l = int(l)
					for data in r.iter_content(chunk_size=1024):
						dl += len(data)
						f.write(data)
						done = int(100 * dl / l)
						pDialog.update(done,'','')
						if pDialog.iscanceled():
							break
				del r
			if done < 100: raise ValueError('Download did not finish!')
		except:
			retry = xbmcgui.Dialog().yesno('[2/'+str(steps)+'] Download Failed','Try again / Thử lại?')
		else:
			CHECK(True)
		if retry:
			DOWNLOAD()

def CHECK(prompt): # 3 of 6 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 3 of '+str(steps)+' ]','Checking / Kiểm tra...','',0,False)
	try:
		z = zipfile.ZipFile(tmp+zip, 'r')
		z.testzip()
	except:
		if prompt:
			retry = xbmcgui.Dialog().yesno('[3/'+str(steps)+'] File Check Failed','Try again / Thử lại?')
		else:
			retry = True
	else:
		EXTRACT(z)
	if retry:
		CLEAN_TMP()
		DOWNLOAD()

def EXTRACT(z): # 4 of 6 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 4 of '+str(steps)+' ]','Extracting / Giải nén...','',0,False)
	try:
		z.extractall(tmp)
		z.close()
	except:
		retry = xbmcgui.Dialog().yesno('[4/'+str(steps)+'] File Extraction Failed','Try again / Thử lại?')
	else:
		shutil.move(tmp+apk,temp+apk) # Move APK file to temporary for staging
		COPY_UPDATES()
	if retry:
		DOWNLOAD()

def COPY_UPDATES(): # 5 of 6 ###############################################################################
	retry = False
	pDialog = POPUP('[ Step 5 of '+str(steps)+' ]','Copying additional / Sao chép thêm...','',0,False)
	success = COPYR(tmp+upd,home,updN)
	if success:
		pDialog = POPUP('[ Step 5 of '+str(steps)+' ]','Clearing cache...','',0,False)
		CLEAN_TMP() # Cleans tmp
		UPDATE_SPMC()
	else:
		retry = xbmcgui.Dialog().yesno('[5/'+str(steps)+'] Files Copy Failed','Try again / Thử lại?')
	if retry:
		COPY_UPDATES()

def UPDATE_SPMC(): # 6 of 6 ###############################################################################
	retry = False
	# Close Home Launcher
	try:
		subprocess.call([cmd_+'/system/bin/am force-stop '+launcher+_cmd], executable='/system/bin/sh', shell=True)
	except:
		pass
	pDialog = POPUP('[ Step 6 of '+str(steps)+' ]','Updating / Đang cập nhật...','',0,False)
	# USER PROMPTED INSTALL FOR LEGACY
	xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.packageinstaller","android.intent.action.INSTALL_PACKAGE","application/vnd.android.package-archive","file:'+temp+apk+'")')
	xbmc.sleep(2000)
	retry = xbmcgui.Dialog().yesno('Confirmation','Operation completed','Hoạt động hoàn thành','','OK','Retry')
	if retry:
		UPDATE_SPMC()

# ------------------------------------------------------------------------------------------------------------------------------------ #

def POPUP(line1,line2,line3,progress,button): ###############################################################################
	pDialog.update(progress,line1,line2,line3)
	pDialog_Window = xbmcgui.Window(10101)
	pDialog_cancelButton = pDialog_Window.getControl(10)
	pDialog_cancelButton.setEnabled(button)
	return pDialog

pDialog = xbmcgui.DialogProgress()
pDialog.create('Please Wait','','','')

def CLEAN_TMP(): ###############################################################################
#	subprocess.call([cmd_+'/system/bin/rm -rf '+tmp+_cmd], executable='/system/bin/sh', shell=True)
	try:
		shutil.rmtree(tmp.rstrip('/'))
	except:
		pass

def MAKE_TMP(): ###############################################################################
	if not os.path.isdir(tmp):
		try:
			os.mkdir(tmp)
			subprocess.call([cmd_+'/system/bin/chown system:sdcard_rw '+tmp+_cmd], executable='/system/bin/sh', shell=True)
			subprocess.call([cmd_+'/system/bin/chmod 755 '+tmp+_cmd], executable='/system/bin/sh', shell=True)
		except:
			xbmcgui.Dialog().ok('Folder Creation Failed','Cannot create Temp folder')

def COPYR(root_src_dir,root_dst_dir,count): ###############################################################################
	file = 0
	pDialog = POPUP('','','',0,False)
	if not os.path.isdir(root_dst_dir):
		subprocess.call([cmd_+'/system/bin/mkdir -p '+root_dst_dir+_cmd], executable='/system/bin/sh', shell=True)
	try:
		p = subprocess.Popen([cmd_+'/system/bin/cp -Rfv '+root_src_dir+'. '+root_dst_dir+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		while True:
			line = p.stdout.readline()
			if not line:
				break
			file = file+1
			done = int(file*100/count)
			pDialog.update(done,'','','')
	except:
		return False
	else:
		return True

def CLEAR(folders,files): ###############################################################################
#Folders
	for i in range(len(folders)):
		if os.path.isdir(folders[i]):
			try:
				shutil.rmtree(folders[i])
			except:
				pass
#Files
	for i in range(len(files)):
		if os.path.isfile(files[i]):
			try:
				os.remove(files[i])
			except:
				pass

# ------------------------------------------------------------------------------------------------------------------------------------ #

www       = 'https://s3.amazonaws.com/itvviet/'
zip       = 'vs150_20160221.zip' # Zip package
apk       = 'vs150_20160220.apk' # Updated APK
upd       = 'vs150/'             # Uncompressed version of misc. files
updN      = 150                  # Count of misc. files
reqMB     = 250                  # Required free space

sdcard    = '/sdcard/'
home      = '/sdcard/Android/data/com.semperpax.spmc/files/.spmc/'; steps = 6
app_cache = os.path.join('/data/data/com.semperpax.spmc/')
tmp       = os.path.join(home, 'tmp/')
temp      = os.path.join(home, 'temp/')
launcher  = 'com.itv.homelauncher'

# Parse *NIX commands for compatibility
cmd_ = 'exec '; _cmd = ''

# Pre-ops clear paths
folders1 = [
	os.path.join(home, 'addons/packages'),
	os.path.join(home, 'userdata/Thumbnails'),
#	os.path.join(home, 'userdata/Database/CDDB'),
#	os.path.join(home, 'cache'),
#	os.path.join(home, 'temp'),
#	os.path.join(app_cache, 'cache'),
	os.path.join(sdcard, 'Download/APKs'),
	os.path.join(sdcard, 'show_box'),
	os.path.join(sdcard, 'SingerKaraoke')
]
files1 = [
#	os.path.join(home, 'userdata/Database/Textures13.db'),
#	os.path.join(home, 'userdata/Database/Addons16.db'),
#	os.path.join(home, 'userdata/Database/Epg8.db'),
#	os.path.join(home, 'userdata/Database/MyMusic48.db'),
#	os.path.join(home, 'userdata/Database/MyVideos90.db'),
#	os.path.join(home, 'userdata/Database/TV26.db')
]