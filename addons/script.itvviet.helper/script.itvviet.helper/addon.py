#!/usr/bin/env python
# -*- coding: utf-8 -*-
IGOR = False # IS THAT YOU?

import xbmc, xbmcaddon, xbmcgui, xbmcplugin
import base64, hashlib, os, platform, requests, shutil, subprocess, sys, urllib, urllib2, zipfile

# Disable requests url3 ssl related warning messages
requests.packages.urllib3.disable_warnings()

AddonTitle    = 'iTV Viet Helper'
local         = xbmcaddon.Addon(id='script.itvviet.helper')
path          = xbmc.translatePath(local.getAddonInfo('path'))
art           = path+'/art'
mainurl       = 'https://s3.amazonaws.com/itvviet'
secondaryurl  = 'https://raw.githubusercontent.com/google/material-design-icons'
defaulticon   = xbmc.translatePath(os.path.join(path,'icon.png'));
defaultfanart = xbmc.translatePath(os.path.join(path,'fanart.jpg'));
HelpVersion   = '2.3'

####################################################################################################
# POPULATE LINKS #
####################################################################################################

def MAIN_MENU():
	if (xbmc.getCondVisibility('system.platform.android') and ((hashlib.sha512(model).hexdigest() in models) or (hashlib.sha512(model).hexdigest() in moreModels))) or IGOR:
		if int(xbmc.getInfoLabel('System.BuildVersion')[:2]) == 14 and hashlib.sha512(model).hexdigest() in models:
			addDir('[COLOR white]Update iTV Viet app -- Cập nhật iTV Việt app[/COLOR]', '015', 515, getArt('com.itvviet.png'), defaultfanart, '')
		if 'com.tvvietnam.androidtv' not in installed_apps and hashlib.sha512(model).hexdigest() in models:
			addDir('[COLOR white]Việt Live[/COLOR] [COLOR yellow]*NEW APP*[/COLOR]', 'com.tvvietnam.androidtv-1.3.apk', 111, getArt('com.tvvietnam.androidtv.png'), defaultfanart, '')
		addDir('[COLOR yellow]INSTALL MORE APPS -- CÀI ĐẶT THÊM APPS[/COLOR]', 'url', 100, getArt2('action/drawable-xxxhdpi/ic_get_app_white_48dp.png'), defaultfanart, '')
		addDir('[COLOR yellowgreen]UNINSTALL APPS -- XÓA BỎ APPS[/COLOR]', 'url', 400, getArt2('action/drawable-xxxhdpi/ic_highlight_off_white_48dp.png'), defaultfanart, '')
		addDir('[COLOR lime]APPLY FIXES -- CHỈNH SỬA[/COLOR]', 'url', 500 , getArt2('action/drawable-xxxhdpi/ic_build_white_48dp.png'), defaultfanart, '')
#		addDir('[COLOR green]SOME CAT - SOME CAT[/COLOR]', 'url', 600 , getArt2(''), defaultfanart, '')
		addDir('', 'url', 0, '', defaultfanart, '')
		addDir('[COLOR cyan]HELP -- TRỢ GIÚP[/COLOR]', 'url', 800 , getArt2('action/drawable-xxxhdpi/ic_help_white_48dp.png'), defaultfanart, '')
		addDir('[COLOR teal]'+model+' | Version '+xbmc.getInfoLabel('Skin.String(Script.Version)')+'[/COLOR]', 'url', 800 , getArt2('action/drawable-xxxhdpi/ic_help_white_48dp.png'), defaultfanart, '')
		addDir('[COLOR teal]Help Menu '+HelpVersion+'[/COLOR]', 'url', 800 , getArt2('action/drawable-xxxhdpi/ic_help_white_48dp.png'), defaultfanart, '')
		addDir('', 'url', 0, '', defaultfanart, '')
		addDir('[COLOR gray]Copyright/DMCA[/COLOR]', 'https://itvviet.com/copyright', 888 , getArt2('action/drawable-xxxhdpi/ic_open_in_browser_white_48dp.png'), defaultfanart, '')
	else:
		HELP()

def INSTALL_APPS():
#	if '' not in installed_apps:
#		addDir('', '.apk', 111, getArt('.png'), defaultfanart, '')
	if 'com.vndynapp.cotuong' not in installed_apps:
		addDir('Cờ Tướng', 'com.vndynapp.cotuong-1.7.apk', 111, getArt('com.vndynapp.cotuong.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Cờ Tướng (Chinese chess, also called Xiangqi) with features:[CR][CR]- Game Mode: One Player, Two Player, Posture, How to solve Posture[CR]- Smart AI with 10 levels and can play either color.[CR]- English, Vietnamese[CR]- Auto save and load[CR][CR]v1.7')
	if 'com.karaoke.singer.vn' not in installed_apps:
		addDir('Karaoke Việt', 'com.karaoke.singer.vn-2.5.apk', 111, getArt('com.karaoke.singer.vn.png'), defaultfanart, 'Sing online in any language with a virtually unlimited selection of songs. Pick from the list of featured songs or search for your favourite artist or track to start singing right away. It\'s easy and free.[CR][CR]v2.5')
	if 'com.vttm.keengsmarttv' not in installed_apps:
		addDir('Keeng TV', 'com.vttm.keengsmarttv-1.0.5.apk', 111, getArt('com.vttm.keengsmarttv.png'), defaultfanart, 'Cài đặt và sử dụng Keeng để thưởng thức kho nhạc hàng triệu bài hát V-pop, K-pop, C-pop, US-UK chất lượng cao 320 kbps và trải nghiệm đầy đủ các tính năng của một mạng xã hội âm nhạc.[CR][CR]Keeng cũng là trang nhạc duy nhất hiện nay có kênh truyền hình giải trí trực tuyến KeengTV phát sóng 24/7 với những chương trình thực tế và các sản phẩm nội dung do chính Keeng Studio sản xuất.[CR][CR]v1.0.5')
	if 'com.ageofmobile.calendar' not in installed_apps:
		addDir('Lịch', 'com.ageofmobile.calendar-2.1.5.apk', 111, getArt('com.ageofmobile.calendar.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Ứng dụng đơn giản giúp bạn tra cứu Âm Lịch/Dương Lịch thuận tiện và nhanh chóng.[CR][CR]A simple Vietnamese Calendar application, for Vietnamese users only.[CR][CR]v2.1.5')
	if 'gov.bbg.rfa' not in installed_apps:
		addDir('Radio Free Asia (RFA)', 'gov.bbg.rfa-3.1.apk', 111, getArt('gov.bbg.rfa.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]The free, *official* Radio Free Asia (RFA) news application serves your Android smartphone or tablet the most up to date and accurate news from around the world.[CR][CR]v3.1')
	if 'com.thaonguyenxanh.radioviet' not in installed_apps:
		addDir('Radio Vietnam', 'com.thaonguyenxanh.radioviet-2.1.1223.apk', 111, getArt('com.thaonguyenxanh.radioviet.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]An online radio app for FREE, letting users listen to radio stations from Vietnam anytime, anywhere.[CR][CR]v2.1.1223')
	if 'com.vn.dic.e.v.ui' not in installed_apps:
		addDir('TFLAT Dictionary', 'com.vn.dic.e.v.ui-5.0.0.apk', 111, getArt('com.vn.dic.e.v.ui.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]English Vietnamese Dictionary is Offline and Free. You can search for new vocabulary everywhere without any Internet connection.[CR][CR]The dictionary includes the following main functions:[CR][CR]- Includes transcriptions and audio pronunciation.[CR]- Function translated sentences, paragraphs from English to Vietnamese and vice versa.[CR]- The words includes synonym and antonym.[CR]- Vietnamese English dictionary contains more than 200,000 words[CR]- With the vocabulary reminder function daily you can learn vocabulary easier.[CR]- Help you to learn 3000 basic English words[CR]- Allow open quickly search window. It supports you to translate while reading English newspapers.[CR][CR]v5.0.0')
	if 'vn.tini.apps.news' not in installed_apps:
		addDir('Tin Tức', 'vn.tini.apps.news-1.0.7.apk', 111, getArt('vn.tini.apps.news.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Tin Tức - Doc Bao Viet là ứng dụng hỗ trợ bạn cập nhật, đọc những tin tức, tin mới nóng hổi nhất từ hơn 70 nguồn báo mạng uy tín. Tin Tức được thiết kế theo phong cách hiện đại mới nhất hiện nay. Ưu điểm nổi bật nhất là tốc độ tải nhanh đến mức kinh ngạc nhờ tận dụng những công nghệ truyền tải dữ liệu mới nhất.[CR][CR]v1.0.7')
	if ('com.vietuu.hlsplayer' not in installed_apps) and (hashlib.sha512(model).hexdigest() in models):
		addDir('UNO IPTV', 'com.vietuu.hlsplayer-1.0.23.apk', 111, getArt('com.vietuu.hlsplayer.png'), defaultfanart, 'Hơn 7 đài truyền hình Việt ngữ theo từng kênh riêng biệt như tin tức, phim bộ, ca nhạc, giải trỉ, vv..[CR][CR]Created to meet the needs of English and Vietnamese-speaking television audiences in the United States and worldwide, UNO IPTV brings you quality Vietnamese and English entertainment programs tailor-made for your preferences.[CR][CR]v1.0.23')
	if ('com.tvvietnam.androidtv' not in installed_apps) and (hashlib.sha512(model).hexdigest() in models):
		addDir('Việt Live', 'com.tvvietnam.androidtv-1.3.apk', 111, getArt('com.tvvietnam.androidtv.png'), defaultfanart, 'Ứng dụng miễn phí giúp bạn theo dõi rất nhiều kênh tivi đặc sắc trong và ngoài nước với chất lượng tốt và đường truyền nhanh, ổn định. Ngoài ra ứng dụng còn giúp các bạn theo dõi các trận bóng đá trực tiếp ở tất cả các giải đấu hàng đầu thế giới.[CR][CR]v1.3')
	if 'com.jsmedia.android.eradio.vietnam' not in installed_apps:
		addDir('Vietnam eRadio', 'com.jsmedia.android.eradio.vietnam-1.8.apk', 111, getArt('com.jsmedia.android.eradio.vietnam.png'), defaultfanart, 'Vietnam eRadio is a free application which lets you listen to online radios on your Android smart phone and tablet. Currently, we serve the following channels: Radio Free Asia, Voice of America, Radio Tiếng Nước Tôi, Radio Saigon Houston, NHK World Radio, Little Saigon Radio, Vietnamese American Broadcasting, Chủ Trương, Nationwide Viet HD Radio, Radio Saigon Dallas 1600AM, Vien Thao Radio, Cherry Radio, Saigon HD Radio, SBS Vietnamese, Dòng Chúa Cứu Thế Việt Nam, Radio France International, ABC Australia, VOV, etc.[CR][CR]v1.8')
	if 'com.viettv24.iptv' not in installed_apps:
		addDir('Viet TV 24', 'com.viettv24.iptv-2.8.apk', 111, getArt('com.viettv24.iptv.png'), defaultfanart, 'For the purpose of serving Vietnamese communities around the world Viet TV24 Media Network wishes to bring you the entertaining moments of comfort every day without paying any fees. This simple application is a tuning engine which is capable of searching all TV channel links available online for free. Now you can watch your online favorite TV programs including news, travel, sports, music, law, movies, comedy...plus TV channels of Vietnamese in the United States (Free to Air) - Vietface, SET, Saigon TV (STV), Little Saigon TV (LSTV)...[CR][CR]v2.8')
	if 'gov.bbg.voa' not in installed_apps:
		addDir('Voice of America (VoA)', 'gov.bbg.voa-3.1.apk', 111, getArt('gov.bbg.voa.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]The free, *official* Voice of America (VOA) mobile / tablet application serves news to your Android smartphone or tablet in 44 languages. VOA operates one of the world\'s largest international news media networks on TV, radio and digital properties with a total weekly audience of more than 200 million people in countries including Africa, The Middle East, Asia and beyond. To do this, VOA employs more than 3,500 journalists around the world, focused on countries without a free or established press or free and open internet access.[CR][CR]v3.1')
	if 'com.zing.mp3' not in installed_apps:
		addDir('Zing Mp3', 'com.zing.mp3-3.7.1.apk', 111, getArt('com.zing.mp3.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Enjoy music with the No.1 music player application for Android in Vietnam.[CR][CR]v3.7.1')
	addDir('Chinese Apps', 'url', 210, getArt('_china.png'), defaultfanart, '')
	addDir('French Apps', 'url', 220, getArt('_france.png'), defaultfanart, '')
	addDir('Thai Apps', 'url', 230, getArt('_thailand.png'), defaultfanart, '')
	addDir('Other Apps', 'url', 290, getArt2('action/drawable-xxxhdpi/ic_android_white_48dp.png'), defaultfanart, '')

def INSTALL_APPS_CN():
	if 'com.shafa.market' not in installed_apps:
		addDir('沙发管家 Shafa Market', 'com.shafa.market-4.4.6.apk', 111, getArt('com.shafa.market.png'), defaultfanart, '为电视盒子提供超多款完美适合在电视盒子上使用的影视，软件，游戏。 千万家庭最信赖的电视应用市场。')
	if 'com.cibn.tv' not in installed_apps:
		addDir('互联网电视 CIBN', 'com.cibn.tv-1.4.apk', 111, getArt('com.cibn.tv.png'), defaultfanart, 'v1.4')
	if 'hdpfans.com' not in installed_apps:
		addDir('高清范 HDPfans', 'hdpfans.com-1.9.6.apk', 111, getArt('hdpfans.com.png'), defaultfanart, 'v1.9.6')
	if 'com.moretv.android' not in installed_apps:
		addDir('电视猫 MoreTV', 'com.moretv.android-2.6.1.apk', 111, getArt('com.moretv.android.png'), defaultfanart, 'v2.6.1')
	if 'com.elinkway.tvlive2' not in installed_apps:
		addDir('电视家 TV plus', 'com.elinkway.tvlive2-2.5.3.apk', 111, getArt('com.elinkway.tvlive2.png'), defaultfanart, 'v2.5.3')
	if 'com.youku.phone' not in installed_apps:
		addDir('优酷 Youku', 'com.youku.phone-4.6.apk', 111, getArt('com.youku.phone.png'), defaultfanart, 'v4.6')

def INSTALL_APPS_FR():
	if 'com.france24.androidapp' not in installed_apps:
		addDir('France 24', 'com.france24.androidapp-2.0.9.4.apk', 111, getArt('com.france24.androidapp.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Top news stories, latest news, breaking news alert, special reports, live TV shows or TV on demand - get free access to France 24\'s coverage of international and French news on your smartphones and tablets, in French, English or Arabic.[CR][CR]v2.0.9.4')
	if 'com.france.INFO' not in installed_apps:
		addDir('France News', 'com.france.INFO-1.0.apk', 111, getArt('com.france.INFO.png'), defaultfanart, 'Suivez l\'actualité en France et dans le monde sur app France 7/7 Actualités. Consultez et recherchez dans la source d\'information la plus complète du web.[CR][CR]v1.0')
#	Spammy
#	if 'com.vinaapps.frenchtv' not in installed_apps:
#		addDir('France TV Channels', 'com.vinaapps.frenchtv-1.0.apk', 111, getArt('com.vinaapps.frenchtv.png'), defaultfanart, '[CR][CR]v1.0')
	if 'tv.tou.android' not in installed_apps:
		addDir('ICI Tou.tv', 'tv.tou.android-2.1.3.45.apk', 111, getArt('tv.tou.android.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]L\'application ICI TOU.TV permet de profiter de l\'offre d\'ICI TOU.TV en mobilité.[CR][CR]ICI TOU.TV offre le rattrapage et la rediffusion du réseau de Radio-Canada, incluant ICI Radio-Canada Télé, ICI RDI, ICI ARTV et ICI Explora; ainsi que d’autres diffuseurs partenaires francophones: Télé-Québec, TFO, TV5, RTBF, RTS et France Télévision.[CR][CR]v2.1.3.45')
	if 'com.radiolight.france' not in installed_apps:
#	Slow loading
#		addDir('Radio France', 'com.radiolight.france-1.1.apk', 111, getArt('com.radiolight.france.png'), defaultfanart, '[CR][CR]v1.1')
#	if 'com.rfi.androidapp' not in installed_apps:
		addDir('RFI', 'com.rfi.androidapp-2.0.apk', 111, getArt('com.rfi.androidapp.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]The RFI news and music app is in French, English and 9 other languages : Cambodian, Chinese, Hausa, Kiswahili, Spanish, Persian, Portuguese, Russian, Vietnamese.[CR][CR]v2.0')
	if 'com.tv5monde.asia' not in installed_apps:
		addDir('TV5MONDE+ Asie', 'com.tv5monde.asia-1.0.2.apk', 111, getArt('com.tv5monde.asia.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]With TV5MONDE+ Asie, enjoy watching your favourite programmes live anytime, anywhere! Sign up on asie.tv5monde.com to enjoy the full application’s content.[CR][CR]v1.0.2')

def INSTALL_APPS_TH():
	if 'com.makathon.tvthailand' not in installed_apps:
		addDir('TV Thailand', 'com.makathon.tvthailand-2.4.14.apk', 111, getArt('com.makathon.tvthailand.png'), defaultfanart, 'Playlist for Watch TV Show, Thai Series, Korea Series, Sitcom, Music Video, Cartoon on Demand from Youtube.')

def INSTALL_APPS_ETC():
#	if '' not in installed_apps:
#		addDir('', '.apk', 111, getArt('.png'), defaultfanart, '')
	if 'com.kbudev.speedtest' not in installed_apps:
		addDir('BeMobile Speed Test', 'com.kbudev.speedtest-3.0.2.apk', 111, getArt('com.kbudev.speedtest.png'), defaultfanart, 'Speed Test will measure your internet Download speed using your active connection from 3 destinations: United States, Europe and Asia. In the normal test, the downloads are performed at the same time to maximize throughput. In the advanced test, speed is shown on a graph for easy interpretation. You can also see the average and maximum speeds for all the destinations.[CR][CR]v3.0.2')
	if 'com.duolingo' not in installed_apps:
		addDir('Duolingo', 'com.duolingo-3.11.2.apk', 111, getArt('com.duolingo.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Learn Spanish, French, German, Italian, Portuguese, Dutch, Irish, Danish, Swedish, and English. Totally fun and 100% free.[CR][CR]v3.11.2')
	if 'com.facebook.katana' not in installed_apps:
		addDir('Facebook', 'com.facebook.katana-43.0.0.29.147.apk', 111, getArt('com.facebook.katana.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Keeping up with friends is faster than ever.[CR] - See what friends are up to[CR] - Share updates, photos and videos[CR] - Get notified when friends like and comment on your posts[CR] - Play games and use your favorite apps[CR][CR]v43.0.0.29.147')
	if 'com.facebook.orca' not in installed_apps:
		addDir('Facebook Messenger', 'com.facebook.orca-47.0.0.28.16.apk', 111, getArt('com.facebook.orca.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Instantly reach the people in your life—for free. Messenger is just like texting, but you don\'t have to pay for every message.[CR][CR]v47.0.0.28.16')
	if 'jp.snowlife01.android.appkiller2' not in installed_apps:
		addDir('Fast Task Killer', 'jp.snowlife01.android.appkiller2-1.2.0.apk', 111, getArt('jp.snowlife01.android.appkiller2.png'), defaultfanart, 'Releases the memory by terminating running and background-waiting app processes. Just one tap the app icon to instantly release memory.[CR][CR]v1.2.0')
	if 'com.lego.legotv' not in installed_apps:
		addDir('LEGO TV', 'com.lego.legotv-2.1.5.apk', 111, getArt('com.lego.legotv.png'), defaultfanart, 'Watch your favourite LEGO® movies directly on your device. This app allows you to watch LEGO movies in both online and offline mode. You can download your favourite LEGO movies to your device and watch them whenever you like.[CR][CR]Important Messaging for Parents: [CR]- The app may include product videos, like TV commercials and product reviews.[CR]- The app includes links to the LEGO.com website, a safe site for kids to have fun.[CR][CR]v2.1.5')
	if 'com.skype.raider' not in installed_apps:
		addDir('Skype', 'com.skype.raider-6.8.0.590.apk', 111, getArt('com.skype.raider.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Say “hello” to friends and family with an instant message, voice or video call on Skype for free. Join the millions of people using Skype today to stay in touch with the people who matter most. There’s so much you can do, right from the palm of your hand.[CR][CR]v6.8.0.590')
	if 'com.sgiggle.production' not in installed_apps:
		addDir('Tango', 'com.sgiggle.production-3.19.177977.apk', 111, getArt('com.sgiggle.production.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Make free voice and video calls. Send free text messages and share photos, videos, and status updates.Swipe profile cards or join a trending conversation to make new friends near and far. Over 300 million people use Tango to connect with people nearby and around the world. Stay in touch with family and friends—the free way.[CR][CR]v3.19.177977')
	if 'com.twitter.android' not in installed_apps:
		addDir('Twitter', 'com.twitter.android-5.83.0.apk', 111, getArt('com.twitter.android.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Find the best of Twitter in an instant with Moments. Follow top stories through immersive pics, clips, and conversations. Get insights and perspectives you won\'t find anywhere else. Twitter is a free app that lets you connect with people, express yourself, and discover more about all the things you love.[CR][CR]v5.83.0')
	if 'com.viber.voip' not in installed_apps:
		addDir('Viber', 'com.viber.voip-5.6.0.2413.apk', 111, getArt('com.viber.voip.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]With Viber, everyone in the world can connect. Freely. More than 606 million Viber users text, make HD-quality phone and video calls, and send photo and video messages worldwide. You can create group messages with up to 200 participants.[CR][CR]v5.6.0.2413')
	if 'com.google.android.youtube' not in installed_apps:
		addDir('YouTube', 'com.google.android.youtube-10.37.58.apk', 111, getArt('com.google.android.youtube.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]Get the official YouTube app for Android phones and tablets. See what the world is watching -- from the hottest music videos to what’s trending in gaming, entertainment, news, and more. Subscribe to channels you love, share with friends, and watch on any device.[CR][CR]With a new design, you can have fun exploring videos you love more easily and quickly than before. Just tap an icon or swipe to switch between recommended videos, your subscriptions, or your account. You can also subscribe to your favorite channels, create playlists, edit and upload videos, express yourself with comments or shares, cast a video to your TV, and more – all from inside the app.[CR][CR]v10.37.58')
	if 'com.google.android.youtube.googletv' not in installed_apps:
		addDir('YouTube for TV', 'com.google.android.youtube.googletv-1.7.6.apk', 111, getArt('com.google.android.youtube.googletv.png'), defaultfanart, 'YouTube your way on the biggest screen in the house, from a playlist of music videos to your favorite comedy channels.[CR][CR]v1.7.6')
	if 'com.google.android.apps.youtube.kids' not in installed_apps:
		addDir('YouTube Kids', 'com.google.android.apps.youtube.kids-1.21.5.apk', 111, getArt('com.google.android.apps.youtube.kids.png'), defaultfanart, '[COLOR yellow]*Mouse required*[/COLOR][CR]The official YouTube Kids app is designed for curious little minds. This free app is delightfully simple and packed full of videos, channels and playlists that kids love. We work hard to make the videos available in the app family friendly, but no algorithm is perfect. If you ever find a video you’re concerned about, please flag it. This helps make YouTube Kids better for everyone.[CR][CR]v1.21.5')

def UNINSTALL_APPS():
#	if '' in installed_apps:
#		addDir('', '', 444, getArt('.png'), defaultfanart, '')
	if 'com.vndynapp.cotuong' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Cờ Tướng', 'com.vndynapp.cotuong', 444, getArt('com.vndynapp.cotuong.png'), defaultfanart, '')
	if 'com.vnonline.dangtv' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] DangTV', 'com.vnonline.dangtv', 444, '', defaultfanart, '')
	if 'com.flyviet.flytv' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] FlyTV', 'com.flyviet.flytv', 444, '', defaultfanart, '')
	if 'com.flyviet.flytv2' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] FlyTV 2', 'com.flyviet.flytv2', 444, '', defaultfanart, '')
	if 'com.karaoke.singer.vn' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Karaoke Việt', 'com.karaoke.singer.vn', 444, getArt('com.karaoke.singer.vn.png'), defaultfanart, '')
	if 'com.vttm.keengsmarttv' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Keeng TV', 'com.vttm.keengsmarttv', 444, getArt('com.vttm.keengsmarttv.png'), defaultfanart, '')
	if 'com.ageofmobile.calendar' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Lịch', 'com.ageofmobile.calendar', 444, getArt('com.ageofmobile.calendar.png'), defaultfanart, '')
	if 'gov.bbg.rfa' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Radio Free Asia (RFA)', 'gov.bbg.rfa', 444, getArt('gov.bbg.rfa.png'), defaultfanart, '')
	if 'com.thaonguyenxanh.radioviet' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Radio Vietnam', 'com.thaonguyenxanh.radioviet', 444, getArt('com.thaonguyenxanh.radioviet.png'), defaultfanart, '')
	if 'com.vn.dic.e.v.ui' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] TFLAT Dictionary', 'com.vn.dic.e.v.ui', 444, getArt('com.vn.dic.e.v.ui.png'), defaultfanart, '')
	if 'vn.tini.apps.news' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Tin Tức', 'vn.tini.apps.news', 444, getArt('vn.tini.apps.news.png'), defaultfanart, '')
	if 'com.vietuu.hlsplayer' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] UNO IPTV', 'com.vietuu.hlsplayer', 444, getArt('com.vietuu.hlsplayer.png'), defaultfanart, '')
#	if 'com.tvvietnam.androidtv' in installed_apps:
#		addDir('[COLOR yellowgreen][Viet][/COLOR] Việt Live', 'com.tvvietnam.androidtv', 444, getArt('com.tvvietnam.androidtv.png'), defaultfanart, '')
	if 'com.jsmedia.android.eradio.vietnam' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Vietnam eRadio', 'com.jsmedia.android.eradio.vietnam', 444, getArt('com.jsmedia.android.eradio.vietnam.png'), defaultfanart, '')
	if 'com.viettv24.iptv' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Viet TV 24', 'com.viettv24.iptv', 444, getArt('com.viettv24.iptv.png'), defaultfanart, '')
	if 'gov.bbg.voa' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Voice of America (VoA)', 'gov.bbg.voa', 444, getArt('gov.bbg.voa.png'), defaultfanart, '')
	if 'com.zing.mp3' in installed_apps:
		addDir('[COLOR yellowgreen][Viet][/COLOR] Zing Mp3', 'com.zing.mp3', 444, getArt('com.zing.mp3.png'), defaultfanart, '')
	if 'com.shafa.market' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 沙发管家 Shafa Market', 'com.shafa.market', 444, getArt('com.shafa.market.png'), defaultfanart, '')
	if 'org.jykds.player' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 91看电视', 'org.jykds.player', 444, '', defaultfanart, '')
	if 'com.cibn.tv' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 互联网电视 CIBN', 'com.cibn.tv', 444, getArt('com.cibn.tv.png'), defaultfanart, '')
	if 'com.fun.tv' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 风行网 Funshion TV', 'com.fun.tv', 444, '', defaultfanart, '')
	if 'hdpfans.com' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 高清范 HDPfans', 'hdpfans.com', 444, getArt('hdpfans.com.png'), defaultfanart, '')
	if 'com.mitools.livetv.testplayer' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] Mitools Live TV', 'com.mitools.livetv.testplayer', 444, '', defaultfanart, '')
	if 'com.moretv.android' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 电视猫 MoreTV', 'com.moretv.android', 444, getArt('com.moretv.android.png'), defaultfanart, '')
	if 'com.elinkway.tvlive2' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 电视家 TV plus', 'com.elinkway.tvlive2', 444, getArt('com.elinkway.tvlive2.png'), defaultfanart, '')
	if 'com.youku.phone' in installed_apps:
		addDir('[COLOR yellowgreen][Chinese][/COLOR] 优酷 Youku', 'com.youku.phone', 444, getArt('com.youku.phone.png'), defaultfanart, '')
	if 'com.france24.androidapp' in installed_apps:
		addDir('[COLOR yellowgreen][French][/COLOR] France 24', 'com.france24.androidapp', 444, getArt('com.france24.androidapp.png'), defaultfanart, '')
	if 'com.france.INFO' in installed_apps:
		addDir('[COLOR yellowgreen][French][/COLOR] France News', 'com.france.INFO', 444, getArt('com.france.INFO.png'), defaultfanart, '')
#	if 'com.vinaapps.frenchtv' in installed_apps:
#		addDir('[COLOR yellowgreen][French][/COLOR] France TV Channels', 'com.vinaapps.frenchtv', 444, getArt('com.vinaapps.frenchtv.png'), defaultfanart, '')
	if 'tv.tou.android' in installed_apps:
		addDir('[COLOR yellowgreen][French][/COLOR] ICI Tou.tv', 'tv.tou.android', 444, getArt('tv.tou.android.png'), defaultfanart, '')
#	if 'com.radiolight.france' in installed_apps:
#		addDir('[COLOR yellowgreen][French][/COLOR] Radio France', 'com.radiolight.france', 444, getArt('com.radiolight.france.png'), defaultfanart, '')
	if 'com.rfi.androidapp' in installed_apps:
		addDir('[COLOR yellowgreen][French][/COLOR] RFI', 'com.rfi.androidapp', 444, getArt('com.rfi.androidapp.png'), defaultfanart, '')
	if 'com.tv5monde.asia' in installed_apps:
		addDir('[COLOR yellowgreen][French][/COLOR] TV5MONDE+ Asie', 'com.tv5monde.asia', 444, getArt('com.tv5monde.asia.png'), defaultfanart, '')
	if 'com.makathon.tvthailand' in installed_apps:
		addDir('[COLOR yellowgreen][Thai][/COLOR] TV Thailand', 'com.makathon.tvthailand', 444, getArt('com.makathon.tvthailand.png'), defaultfanart, '')
	if 'com.yodo1.crossyroad' in installed_apps:
		addDir('[COLOR yellowgreen][Game][/COLOR] Crossy Road', 'com.yodo1.crossyroad', 444, '', defaultfanart, '')
	if 'net.hexage.radiant.lite' in installed_apps:
		addDir('[COLOR yellowgreen][Game][/COLOR] Radiant', 'net.hexage.radiant.lite', 444, '', defaultfanart, '')
	if 'com.waxrain.airplaydmr' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Airpin', 'com.waxrain.airplaydmr', 444, '', defaultfanart, '')
	if 'com.kbudev.speedtest' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] BeMobile Speed Test', 'com.kbudev.speedtest', 444, getArt('com.kbudev.speedtest.png'), defaultfanart, '')
	if 'com.duolingo' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Duolingo', 'com.duolingo', 444, getArt('com.duolingo.png'), defaultfanart, '')
	if 'com.facebook.katana' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Facebook', 'com.facebook.katana', 444, getArt('com.facebook.katana.png'), defaultfanart, '')
	if 'com.facebook.orca' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Facebook Messenger', 'com.facebook.orca', 444, getArt('com.facebook.orca.png'), defaultfanart, '')
#	System App
#	if 'jp.snowlife01.android.appkiller2' in installed_apps:
#		addDir('[COLOR yellowgreen][Other][/COLOR] Fast Task Killer', 'jp.snowlife01.android.appkiller2', 444, getArt('jp.snowlife01.android.appkiller2.png'), defaultfanart, '')
	if 'com.cdteam.homecinema.ui' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] HD Cinema', 'com.cdteam.homecinema.ui', 444, '', defaultfanart, '')
	if 'com.lego.legotv' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] LEGO TV', 'com.lego.legotv', 444, getArt('com.lego.legotv.png'), defaultfanart, '')
	if 'com.skype.raider' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Skype', 'com.skype.raider', 444, getArt('com.skype.raider.png'), defaultfanart, '')
	if 'com.sgiggle.production' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Tango', 'com.sgiggle.production', 444, getArt('com.sgiggle.production.png'), defaultfanart, '')
	if 'com.twitter.android' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Twitter', 'com.twitter.android', 444, getArt('com.twitter.android.png'), defaultfanart, '')
	if 'com.viber.voip' in installed_apps:
		addDir('[COLOR yellowgreen][Other][/COLOR] Viber', 'com.viber.voip', 444, getArt('com.viber.voip.png'), defaultfanart, '')
#	System App
#	if 'com.google.android.apps.youtube.kids' in installed_apps:
#		addDir('[COLOR yellowgreen][Other][/COLOR] YouTube Kids', 'com.google.android.apps.youtube.kids', 444, getArt('com.google.android.apps.youtube.kids.png'), defaultfanart, '')

def FIXES():
# UPDATES
	if installed_youtube != '1.0.5.6':
		addDir('[COLOR lime][001][/COLOR] Fix YouTube App', 'com.google.android.youtube.googletv-1.0.5.6.apk', 111, getArt('com.google.android.youtube.googletv.png'), defaultfanart, '')
	if installed_karaoke != '2.5':
		addDir('[COLOR lime][002][/COLOR] Fix Karaoke App', 'com.karaoke.singer.vn-2.5.apk', 111, getArt('com.karaoke.singer.vn.png'), defaultfanart, '')
	if int(xbmc.getInfoLabel('System.BuildVersion')[:2]) == 14 and hashlib.sha512(model).hexdigest() in models:
		addDir('[COLOR lime][015][/COLOR] Update iTV Viet App', '015', 515, getArt('com.itvviet.png'), defaultfanart, '')
#		if 'com.itv.moreapps' not in installed_apps:
#			addDir('[COLOR lime]-- Fix More Apps[/COLOR]', 'com.itv.moreapps.apk', 111, getArt('com.itv.moreapps.png'), defaultfanart, '')
# ITV TWEAKS
#	addDir('[COLOR lime][501][/COLOR] Enable Zero Cache', '', 501, '', defaultfanart, '')
	addDir('[COLOR lime][502][/COLOR] Delete Adult Content', '502', 505, '', defaultfanart, '')
	if (not xbmc.getCondVisibility('System.HasLocks')) or (xbmc.getCondVisibility('System.HasLocks') and xbmc.getCondVisibility('System.IsMaster')):
		addDir('[COLOR lime][503][/COLOR] Restore Adult Content', '503', 505, '', defaultfanart, '')
	addDir('[COLOR lime][504][/COLOR] Fix Phoenix', '504', 505, '', defaultfanart, '')
	addDir('[COLOR lime][505][/COLOR] Fix SportsDevil', '505', 505, '', defaultfanart, '')
# HARDWARE
	if hashlib.sha512(model).hexdigest() == '7982c07f5eacf0b40b773ade397af5d86875aa87e5b21c68269e5c5aca00b3f46ff0539bc5b4359a8b9381a34a770ae346f76a6a1f94ba19be3987b06108f71d':
		addDir('[COLOR lime][801][/COLOR] Fix Optical/Digital Audio Setting', 'com.mbx.settingsmbox_1k-1.3.apk', 111, getArt('com.mbx.settingsmbox.png'), defaultfanart, '')
	if hashlib.sha512(model).hexdigest() == '6fc4b9e7970d2570786a47aa024c250a700947e347a05cfec46587fca6301199cbe5d6fc4316e411b8c0b007682256d230c01c5e15ab953fc2aa40d2e5dd338b':
		addDir('[COLOR lime][801][/COLOR] Fix Optical/Digital Audio Setting', 'com.mbx.settingsmbox_4kv2-1.3.apk', 111, getArt('com.mbx.settingsmbox.png'), defaultfanart, '')
# OTHER
	if 'com.waxrain.airplaydmr' not in installed_apps:
		addDir('[COLOR lime][901][/COLOR] Install AirPlay', 'airplay.apk', 111, getArt('airplay.png'), defaultfanart, '')

def HELP():
	addDir('Open iTV Viet Help website', 'http://trogiup.itvviet.com', 888 , getArt2('action/drawable-xxxhdpi/ic_open_in_browser_white_48dp.png'), defaultfanart, '')
	addDir('Go to iTVViet.com website', 'https://itvviet.com', 888 , getArt2('action/drawable-xxxhdpi/ic_open_in_browser_white_48dp.png'), defaultfanart, '')
	addDir('', 'url', 0, '', defaultfanart, '')
	addDir('[COLOR cyan]Tel:[/COLOR] 1-855-479-6202 (Canada/USA Toll-Free)', 'url', 808 , getArt2('communication/drawable-xxxhdpi/ic_call_white_48dp.png'), defaultfanart, '')
	addDir('[COLOR cyan]Tel:[/COLOR] +1-647-479-6202 (International)', 'url', 808 , getArt2('communication/drawable-xxxhdpi/ic_call_white_48dp.png'), defaultfanart, '')
	addDir('[COLOR cyan]Email:[/COLOR] hello@itvviet.com', 'url', 808 , getArt2('communication/drawable-xxxhdpi/ic_email_white_48dp.png'), defaultfanart, '')
	addDir('', 'url', 0, '', defaultfanart, '')
	addDir('[COLOR gray]Copyright/DMCA[/COLOR]', 'https://itvviet.com/copyright', 888 , getArt2('action/drawable-xxxhdpi/ic_open_in_browser_white_48dp.png'), defaultfanart, '')

def getArt(n): 
	if os.path.isfile(os.path.join(art,n)) == True: return os.path.join(art,n)
	else: return mainurl+'/apk/'+n

def getArt2(n): 
	if os.path.isfile(os.path.join(art,n)) == True: return os.path.join(art,n)
	else: return secondaryurl+'/master/'+n

def addDir(name, url, mode, iconimage, fanart, description):
	u = sys.argv[0]+'?url='+urllib.quote_plus(url)+'&mode='+str(mode)+'&name='+urllib.quote_plus(name)+'&iconimage='+urllib.quote_plus(iconimage)+'&fanart='+urllib.quote_plus(fanart)+'&description='+urllib.quote_plus(description); ok = True
	liz = xbmcgui.ListItem(name, iconImage = 'DefaultFolder.png', thumbnailImage = iconimage); liz.setInfo(type = 'Video', infoLabels = {'Title':name, 'Plot':description}); liz.setProperty('Fanart_Image', fanart);
	if mode == 0 or mode == 111 or mode == 444 or mode == 505 or mode == 515 or mode == 808 or mode == 888:
		  ok = xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = u, listitem = liz, isFolder = False)
	else: ok = xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = u, listitem = liz, isFolder = True)
	return ok

def get_params(param = []):
	paramstring = sys.argv[2]
	
	if len(paramstring) >= 2:
		params = sys.argv[2]; cleanedparams = params.replace('?', '')
		if (params[len(params)-1] == '/'): params = params[0:len(params)-2]
		pairsofparams = cleanedparams.split('&'); param = {}
		for i in range(len(pairsofparams)):
			 splitparams = {}; splitparams = pairsofparams[i].split('=')
			 if (len(splitparams)) == 2: param[splitparams[0]] = splitparams[1]
	return param


####################################################################################################
# GET APP INVENTORY #
# Required arg for Android: executable='/system/bin/sh'
####################################################################################################

def INVENTORY():
	# List APKS and APP packages
	try:
		installed_apps = subprocess.Popen([cmd_+'/system/bin/pm list packages'+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n').splitlines()
	except:
		installed_apps = []
	else:
		for i in range(len(installed_apps)):
			if platform_release == '3.10.0' or platform_release == '3.0.36+':
				installed_apps[i] = installed_apps[i].partition(':')[2]
			else:
				installed_apps[i] = installed_apps[i].partition('=')[2]
	# Check YouTube version
	try:
		installed_youtube = subprocess.Popen([cmd_+'/system/bin/dumpsys package com.google.android.youtube.googletv | grep versionName'+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n')
		installed_youtube = installed_youtube.replace('    versionName=','')
	except:
		pass
	# Check Karaoke version
	try:
		installed_karaoke = subprocess.Popen([cmd_+'/system/bin/dumpsys package com.karaoke.singer.vn | grep versionName'+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n')
		installed_karaoke = installed_karaoke.replace('    versionName=','')
	except:
		pass

	return installed_apps, installed_youtube, installed_karaoke

####################################################################################################
# DOWNLOAD APK FILES #
####################################################################################################

www      = 'https://s3.amazonaws.com/itvviet/apk/'
dld      = '/sdcard/Download/'
tmp      = dld+'APKs/'

def GRAB(apk,app):
	if not os.path.isdir(dld):
		os.mkdir(dld)
		subprocess.call([cmd_+'/system/bin/chown system:sdcard_rw '+dld+_cmd], executable='/system/bin/sh', shell=True)
		subprocess.call([cmd_+'/system/bin/chmod 755 '+dld+_cmd], executable='/system/bin/sh', shell=True)
	if not os.path.isdir(tmp):
		os.mkdir(tmp)
		subprocess.call([cmd_+'/system/bin/chown system:sdcard_rw '+tmp+_cmd], executable='/system/bin/sh', shell=True)
		subprocess.call([cmd_+'/system/bin/chmod 755 '+tmp+_cmd], executable='/system/bin/sh', shell=True)
	# DOWNLOADS THE PRECIOUS
	with open(tmp+apk, "wb") as f:
		pDialog = xbmcgui.DialogProgress()
		pDialog.create('Please Wait','Starting download','Bắt đầu tải...')
		r = requests.get(www+apk, stream=True)
		l = r.headers.get('content-length')
		if not l is None:
			dl = 0
			l = int(l)
			for data in r.iter_content(chunk_size=1024):
				dl += len(data)
				f.write(data)
				done = int(100 * dl / l)
				pDialog.update(done,'Downloading / Đang tải', app+'...')
				if pDialog.iscanceled():
					break
	pDialog.close()
	del r
	return done

####################################################################################################
# INSTALL / UNINSTALL APK FILES #
# https://android.googlesource.com/platform/packages/apps/PackageInstaller/+/android-4.2.2_r1/AndroidManifest.xml
####################################################################################################

def INSTALL(apk,app):
	prompt = xbmcgui.Dialog().yesno('Confirmation','Install / Cài đặt',app+'?')
	if prompt:
		retry = False
		try:
			done = GRAB(apk,app)
			if done < 100:
				raise ValueError('Download did not finish!')
		except:
			retry = xbmcgui.Dialog().yesno('Download Failed','Try again / Thử lại?')
		else:
			# DIRECT ROOTED INSTALL
			try:
				pDialog = xbmcgui.DialogProgress()
				pDialog.create('Please Wait', 'Installing / Đang cài')
				install_result = subprocess.Popen([cmd_+'/system/bin/pm install -r -d '+tmp+apk+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n').splitlines()
				pDialog.close()
				if not install_result[1]:
					raise ValueError('App was not installed!')
			except:
			# USER PROMPTED INSTALL
				xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.packageinstaller","android.intent.action.INSTALL_PACKAGE","application/vnd.android.package-archive","file:'+tmp+apk+'")')
				xbmc.sleep(2000)
				retry = xbmcgui.Dialog().yesno('Confirmation','Operation completed','Hoạt động hoàn thành','','OK','Retry')
			else:
				if install_result[1] == 'Success':
					xbmcgui.Dialog().ok('Success',app+' installed / được cài đặt!')
				else:
					xbmcgui.Dialog().ok('Error',install_result[1])
			finally:
				xbmcgui.Dialog().notification('Refreshing Apps','Please wait...', xbmcgui.NOTIFICATION_INFO, 5000, False)
				pac = apk.partition('-')[0].replace('.apk','')
				installed_apps.extend(pac)
				xbmc.executebuiltin('Container.Refresh')
		if retry:
			INSTALL(apk,app)

def UNINSTALL(pac,app):
	prompt = xbmcgui.Dialog().yesno('Confirmation','Remove / Xóa bỏ',app+'?')
	if prompt:
		# DIRECT ROOTED UNINSTALL
		try:
			pDialog = xbmcgui.DialogProgress()
			pDialog.create('Please Wait', 'Removing / Đang xóa...')
			uninstall_result = subprocess.Popen([cmd_+'/system/bin/pm uninstall '+pac+_cmd], executable='/system/bin/sh', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].rstrip('\n')
			pDialog.close()
			if not uninstall_result:
				raise ValueError('App was not removed!')
		except:
		#USER PROMPTED UNINSTALL
		#ADB SHELL: am start -a android.intent.action.DELETE -n com.android.packageinstaller/.UninstallerActivity package:somepackage.apk
			xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.packageinstaller","android.intent.action.DELETE","","package:'+pac+'")')
			xbmc.sleep(2000)
			retry = xbmcgui.Dialog().ok('Confirmation','Operation completed','Hoạt động hoàn thành')
		else:
			if uninstall_result == 'Success':
				xbmcgui.Dialog().ok('Success',app+' removed / được xóa bỏ!')
			else:
				xbmcgui.Dialog().ok('Error',uninstall_result)
		finally:
			xbmcgui.Dialog().notification('Refreshing Apps','Please wait...', xbmcgui.NOTIFICATION_INFO, 5000, False)
			installed_apps.remove(pac)
			xbmc.executebuiltin('Container.Refresh')

####################################################################################################
# LAUNCH WEBSITE #
####################################################################################################

def LAUNCH(url):
	xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.chrome","android.intent.action.VIEW","","'+url+'")')

####################################################################################################
# COPY FOLDERS #
####################################################################################################

def COPY_FOLDERS(src,dst,name):
	prompt = xbmcgui.Dialog().yesno('Confirmation',name+'?')
	if prompt:
		if os.path.isfile(os.path.join(src,'copy_folders.txt')):
			with open(os.path.join(src,'copy_folders.txt')) as cf:
				cfolders = cf.read().splitlines()
			cf.close()
			for i in range(len(cfolders)):
				# Avoid mistakenly overwriting important files ***
				if all(cfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
					if os.path.isdir(os.path.join(dst,cfolders[i])):
						try:
							shutil.rmtree(os.path.join(dst,cfolders[i]))
						except:
							pass
					try:
						shutil.copytree(os.path.join(src,cfolders[i]), os.path.join(dst,cfolders[i]))
					except:
						pass
		xbmcgui.Dialog().ok('Confirmation','Operation completed','Hoạt động hoàn thành')

####################################################################################################
# GATHER SYSTEM INTEL #
####################################################################################################

xbmc_friendlyname    = xbmc.getInfoLabel('System.FriendlyName')
xbmc_buildversion    = xbmc.getInfoLabel('System.BuildVersion')

platform_system      = platform.system().rstrip('\n')
platform_release     = platform.release().rstrip('\n')
platform_machine     = platform.machine().rstrip('\n')

# Parse *NIX commands for compatibility
if platform_release == '3.0.36+':
	cmd_ = 'exec '; _cmd = ''
else:
	cmd_ = 'exec su -c "'; _cmd = '"'

if platform.system() == 'Linux':
	try:
		model             = subprocess.Popen(['/system/bin/getprop', 'ro.product.model'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		brand             = subprocess.Popen(['/system/bin/getprop', 'ro.product.brand'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		device            = subprocess.Popen(['/system/bin/getprop', 'ro.product.device'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		board             = subprocess.Popen(['/system/bin/getprop', 'ro.board.platform'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		manufacturer      = subprocess.Popen(['/system/bin/getprop', 'ro.product.manufacturer'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		build_id          = subprocess.Popen(['/system/bin/getprop', 'ro.build.display.id'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		build_description = subprocess.Popen(['/system/bin/getprop', 'ro.build.description'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		build_fingerprint = subprocess.Popen(['/system/bin/getprop', 'ro.build.fingerprint'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
	except:
		device = 'Unknown'
else:
	model             = '...'
	brand             = '...'
	device            = '...'
	board             = '...'
	manufacturer      = '...'
	build_id          = '...'
	build_description = '...'
	build_fingerprint = '...'
	if platform.system() == 'Darwin':
		try:
			device = subprocess.Popen(['/usr/sbin/sysctl', '-n', 'machdep.cpu.brand_string'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		except:
			device = 'Unknown'
	else:
		device = platform.processor()

####################################################################################################
# ACTIVATE SENTRY #
####################################################################################################

home     = xbmc.translatePath('special://home/')
script   = home + 'addons/script.itvviet.helper/'
rsrc     = script + 'rsrc/'

friendly = [
'9e6ffefdcea6f239f5cde62264e5b6e14308fabd8cd76566cf169a81f4fed2eb86d389818782ed08e05bb90f0c9416f82fb4671678966e6a2ada836ea17ac482',
'7b0b77de353ecd1df21cb32ee77b96e966933968ed58c67aa21deecdf1e0ba1ea714cdc3c11d9039f16d6c3a1b39919be4a7faeb671056f491585f59e657b00a',
'ed6b2dd43dc520a21c4d92f28174e1f0ba0996c55516e6661f351fcea2502269b83e45e9d385228b8a1d6208f0c127378b762e876a5e2e5294152d4858948510',
'a7e06d81717d6236d5aa8515bf1c2755063c39e4de5ab361d276d08aba42ac02e8572c7e0cbe662b23c9c88e5eb59135f8a2f427533f9eebfd93b8242a415db8',
'31dd5457e8029188d3fe7c070280bdf477eb2c48bf441f8c323270bc05759ca7a723c2d7783b3cab89afee436b021909befbe1cc818f433ba6028a421f8e7de7',
'e20416cbe4b0e1c04a9cb54ec736bcbffeacc092f31cc81cb21c00a090d3297ff58dc326232193ab405fcb4bdfbc6d0c749c54a6c047b19b88e097e97f846421'
]
models   = [
'1d8c4ece2520039f5f0928ba0d8b8f3f01dfc426ec2db07e57fe36992a3d4a18dcdd13cce4659bddddd34410a530628752f69a006b58ff3215c7e4576f41dc75',
'7982c07f5eacf0b40b773ade397af5d86875aa87e5b21c68269e5c5aca00b3f46ff0539bc5b4359a8b9381a34a770ae346f76a6a1f94ba19be3987b06108f71d',
'83db8a25715370079a0c12fc3ff2863e0a572e5457298ce5b300aec32fb43704bbdec6247be051a27aeb637aa57541fd23b4ce6b47c5a950930832120d0d7575',
'6fc4b9e7970d2570786a47aa024c250a700947e347a05cfec46587fca6301199cbe5d6fc4316e411b8c0b007682256d230c01c5e15ab953fc2aa40d2e5dd338b'
]
models2   = [
'7982c07f5eacf0b40b773ade397af5d86875aa87e5b21c68269e5c5aca00b3f46ff0539bc5b4359a8b9381a34a770ae346f76a6a1f94ba19be3987b06108f71d',
'83db8a25715370079a0c12fc3ff2863e0a572e5457298ce5b300aec32fb43704bbdec6247be051a27aeb637aa57541fd23b4ce6b47c5a950930832120d0d7575',
'6fc4b9e7970d2570786a47aa024c250a700947e347a05cfec46587fca6301199cbe5d6fc4316e411b8c0b007682256d230c01c5e15ab953fc2aa40d2e5dd338b'
]
moreModels   = [
'63ec5edcff3af5a00f3c4ca83e4427e85035b084ee6b395e1600a3ed326032816da9dfdc7293c142352d9d2d8fab759585763f453483813c55e9fa523c80419c',
'3002b9ec26a1b2695213bb0a0fd65d8eceb8b97b04ee7e195a2d7c215dcc4ddefd0abe85432511db152b53d3df34a199718d015e09189bb3c1b669cc048758b6'
]

if not IGOR:
	status  = 0; missing = ''

	with open(script + 'src') as cf:
		cfolders = cf.read().splitlines()
	cf.close()

	for i in range(len(cfolders)):
		cfolders[i] = base64.urlsafe_b64decode(cfolders[i])
		if not os.path.isdir(home + cfolders[i]):
			status -= 1
			missing = missing + cfolders[i] + ' '

	if (status < 0):
		if hashlib.sha512(xbmc.getInfoLabel('System.FriendlyName')).hexdigest() not in friendly:
			shutil.rmtree(home,ignore_errors=True)
		else:
			pass
			#################################
			#								#
			#		COPY OVER MISSING		#
			#								#
			#################################

	if not xbmc.getCondVisibility('system.platform.android'):
		shutil.rmtree(home,ignore_errors=True)

####################################################################################################

if xbmc.getCondVisibility('system.platform.android'):
	installed_apps, installed_youtube, installed_karaoke = INVENTORY()
else:
# For debugging
	installed_apps = []; installed_youtube = []; installed_karaoke = [];

params = get_params(); url = None; name = None; mode = None; iconimage = None; fanart = None

try:	url = urllib.unquote_plus(params['url'])
except: pass
try:	name = urllib.unquote_plus(params['name'])
except: pass
try:	iconimage = urllib.unquote_plus(params['iconimage'])
except: pass
try:	mode = int(params['mode'])
except: pass
try:	fanart = urllib.unquote_plus(params['fanart'])
except: pass

print 'Mode: '+str(mode); print 'URL: '+str(url); print 'Name: '+str(name); print 'IconImage: '+str(iconimage)

if mode == None or url == None or len(url)<1: MAIN_MENU(); xbmc.executebuiltin('Container.SetViewMode(50)')
elif mode == 100: INSTALL_APPS(); xbmc.executebuiltin('Container.SetViewMode(55)')
elif mode == 111: INSTALL(url, name)
elif mode == 210: INSTALL_APPS_CN(); xbmc.executebuiltin('Container.SetViewMode(55)')
elif mode == 220: INSTALL_APPS_FR(); xbmc.executebuiltin('Container.SetViewMode(55)')
elif mode == 230: INSTALL_APPS_TH(); xbmc.executebuiltin('Container.SetViewMode(55)')
elif mode == 290: INSTALL_APPS_ETC(); xbmc.executebuiltin('Container.SetViewMode(55)')
elif mode == 400: UNINSTALL_APPS(); xbmc.executebuiltin('Container.SetViewMode(50)')
elif mode == 444: UNINSTALL(url, name)
elif mode == 500: FIXES(); xbmc.executebuiltin('Container.SetViewMode(50)')
elif mode == 505: COPY_FOLDERS(rsrc+url+'/',home,name);
elif mode == 515:
	sys.path.append(rsrc)
	if hashlib.sha512(model).hexdigest() == '1d8c4ece2520039f5f0928ba0d8b8f3f01dfc426ec2db07e57fe36992a3d4a18dcdd13cce4659bddddd34410a530628752f69a006b58ff3215c7e4576f41dc75':
		from rsrc.fiveonefiveb import UPDATETO15 as UPDATETO15
	else:
		from rsrc.fiveonefive import UPDATETO15 as UPDATETO15
	UPDATETO15()
elif mode == 800: HELP(); xbmc.executebuiltin('Container.SetViewMode(50)')
elif mode == 888: LAUNCH(url)

xbmcplugin.endOfDirectory(int(sys.argv[1]))