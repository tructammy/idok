plugin.video.snagfilms
================

Kodi Addon for Snagfilms website

Version 3.0.1 Isengard version - separate scraper
version 1.0.11 website changes / use hls m3u8
Version 1.0.10 Website changes
Version 1.0.9 Website changes
Version 1.0.8 Website changes
Version 1.0.7 website change, added retry on html request
Version 1.0.6  added popular, most recent sort types
Version 1.0.5 added search
Version 1.0.4 added gzip processing for all url requests
version 1.0.3 minor bug cleanup
version 1.0.2 initial release

