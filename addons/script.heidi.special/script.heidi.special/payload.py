#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc
import os, shutil, distutils.dir_util

src      = xbmc.translatePath('special://home/addons/script.heidi.special/')
dst      = xbmc.translatePath('special://home/')
rsc      = src + xbmc.getInfoLabel('System.BuildVersion')[:2] + '/'

####################################################################################################

def kill():
	for i in range(14,17):
		try: shutil.rmtree(src + str(i) + '/')
		except: pass

if os.path.isdir(rsc):
	#
	# Copy folders ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(rsc, 'copy_folders.txt')):
		with open(os.path.join(rsc, 'copy_folders.txt')) as cf:
			cfolders = cf.read().splitlines()
		cf.close()

		for i in range(len(cfolders)):
			# Avoid mistakenly overwriting important files ***
			if all(cfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
				if os.path.isdir(os.path.join(dst, cfolders[i])):
					try:
						shutil.rmtree(os.path.join(dst, cfolders[i]))
					except:
						pass
				try:
					shutil.move(os.path.join(rsc, cfolders[i]), os.path.join(dst, cfolders[i]))
				except:
					pass

	#
	# Copy files ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(rsc, 'copy_files.txt')):
		with open(os.path.join(rsc, 'copy_files.txt')) as cfi:
			cfiles = cfi.read().splitlines()
		cfi.close()

		for i in range(len(cfiles)):
			if os.path.isfile(os.path.join(dst, cfiles[i])):
				try:
					os.remove(os.path.join(dst, cfiles[i]))
				except:
					pass
			try:
				shutil.move(os.path.join(rsc, cfiles[i]), os.path.join(dst, cfiles[i]))
			except:
				pass

	#
	# Kill payload ----------------------------------------------------------------------------------------------------
	#
	kill()

if int(xbmc.getInfoLabel('System.BuildVersion')[:2]) not in range(14,17):
	kill()

####################################################################################################