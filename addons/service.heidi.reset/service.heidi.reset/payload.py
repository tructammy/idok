import xbmc, os, shutil, distutils.dir_util

def Deploy():
	src = xbmc.translatePath('special://home/addons/service.heidi.reset/payload/')
	dst = xbmc.translatePath('special://home/')

	####################################################################################################

	#
	# Copy folders ----------------------------------------------------------------------------------------------------
	#
	with open(src + 'copy_folders.txt') as cf:
		cfolders = cf.read().splitlines()
	cf.close()

	for i in range(len(cfolders)):
		# Avoid mistakenly overwriting important files ***
		if all(cfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
		# Checks if folder exists otherwise copies over
			if not os.path.isdir(dst + cfolders[i]):
				try:
					shutil.copytree(src + cfolders[i], dst + cfolders[i])
				except:
					pass

	#
	# Copy files ----------------------------------------------------------------------------------------------------
	#
	with open(src + 'copy_files.txt') as cfi:
		cfiles = cfi.read().splitlines()
	cfi.close()

	for i in range(len(cfiles)):
	# Checks if file exists otherwise copies over
		if not os.path.isfile(dst + cfiles[i]):
			try:
				shutil.copy(src + cfiles[i], dst + cfiles[i])
			except:
				pass

	####################################################################################################
