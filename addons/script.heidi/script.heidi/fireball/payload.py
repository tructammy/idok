#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc, xbmcgui
import os, shutil

####################################################################################################

def Payload(src, dst):

	#
	# Delete folders ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'delete_folders.txt')):
		with open(os.path.join(src, 'delete_folders.txt')) as df:
			dfolders = df.read().splitlines()
		df.close()

		for i in range(len(dfolders)):
			# Avoid mistakenly deleting important files ***
			if all(dfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
				if os.path.isdir(os.path.join(dst, dfolders[i])):
					try:
						shutil.rmtree(os.path.join(dst, dfolders[i]))
					except:
						pass

	#
	# Copy folders ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'copy_folders.txt')):
		with open(os.path.join(src, 'copy_folders.txt')) as cf:
			cfolders = cf.read().splitlines()
		cf.close()

		for i in range(len(cfolders)):
			# Avoid mistakenly overwriting important files ***
			if all(cfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
				if os.path.isdir(os.path.join(dst, cfolders[i])):
					try:
						shutil.rmtree(os.path.join(dst, cfolders[i]))
					except:
						pass
				try:
					shutil.move(os.path.join(src, cfolders[i]), os.path.join(dst, cfolders[i]))
				except:
					pass

	#
	# Delete files ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'delete_files.txt')):
		with open(os.path.join(src, 'delete_files.txt')) as dfi:
			dfiles = dfi.read().splitlines()
		dfi.close()

		for i in range(len(dfiles)):
			if os.path.isfile(os.path.join(dst, dfiles[i])):
				try:
					os.remove(os.path.join(dst, dfiles[i]))
				except:
					pass

	#
	# Copy files ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'copy_files.txt')):
		with open(os.path.join(src, 'copy_files.txt')) as cfi:
			cfiles = cfi.read().splitlines()
		cfi.close()

		for i in range(len(cfiles)):
			if os.path.isfile(os.path.join(dst, cfiles[i])):
				try:
					os.remove(os.path.join(dst, cfiles[i]))
				except:
					pass
			try:
				shutil.move(os.path.join(src, cfiles[i]), os.path.join(dst, cfiles[i]))
			except:
				pass