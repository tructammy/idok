####################################################################################################

ver      = '4.xx'

class Announcement(pyxbmct.AddonFullWindow):
	def __init__(self, viet):
		if viet: #Tieng Viet
			self.viet = True
			header = 'THÔNG BÁO - ITV VIỆT v' + ver
			self.text = """Hiện trong iTV Việt có chương trình mới tự cài đặt hoặc bỏ ra những APPS nào bạn muốn:

- [COLOR yellow]VIỆT LIVE[/COLOR] có hơn 100 đài truyền hình tại Việt Nam và đá banh trực tiếp

- YouTube mới thay thế cho YouTube cũ / Karaoke cài đặt lại (nếu phiên bản của bạn không hoạt động nữa)

- Và thêm những APPS khác...

Để sử dụng tính năng này, hãy vào menu ở phía bên TRÁI và chọn HELP MENU"""
			self.contact = """Phone: 1-855-479-6202 / +1-647-479-6202
Email: hello@itvviet.com"""
			self.buttonL_text = 'BỎ QUA'
			self.buttonC_text = 'OK, VÀO NGAY'
			self.buttonR_text = 'ENGLISH'

		else: #English
			self.viet = False
			header = 'ITV VIET v' + ver + ' - ANNOUNCEMENT'
			self.text = """Now you can install and uninstall apps directly within the iTV Viet program:

- Introducing [COLOR yellow]VIET LIVE[/COLOR]: watch over 100 TV channels from Vietnam, as well as the latest soccer/football games

- New versions of the YouTube and Karaoke apps are available for download (if your version is no longer working)

- Plus other apps...

To access this feature, go to the hidden menu on the LEFT side and choose HELP MENU"""
			self.contact = """Phone: 1-855-479-6202 / +1-647-479-6202
Email: hello@itvviet.com"""
			self.buttonL_text = 'CLOSE'
			self.buttonC_text = 'OK, BROWSE NOW'
			self.buttonR_text = 'TIẾNG VIỆT'
		super(Announcement, self).__init__(header)
		self.setBackground(dst + 'addons/skin.' + script.lower() + '/backgrounds/System.jpg')
		self.setGeometry(1000, 550, 9, 11)
		self.set_controls()
		self.set_navigation()
		self.connect(pyxbmct.ACTION_PREVIOUS_MENU, self.close)
		self.connect(pyxbmct.ACTION_NAV_BACK, self.close)

	def set_controls(self):
		self.textbox = pyxbmct.TextBox(font='rss', textColor='0xFFFFFFFF'); self.placeControl(self.textbox, 0, 1, rowspan=6, columnspan=9); self.textbox.setText(self.text)
		#
		self.contact = pyxbmct.Label(self.contact, alignment=pyxbmct.ALIGN_CENTER); self.placeControl(self.contact, 7, 1, rowspan=2, columnspan=9)
		#
		self.buttonL = pyxbmct.Button(self.buttonL_text); self.placeControl(self.buttonL, 6, 2, columnspan=2)
		self.buttonC = pyxbmct.Button(self.buttonC_text); self.placeControl(self.buttonC, 6, 4, columnspan=3)
		self.buttonR = pyxbmct.Button(self.buttonR_text); self.placeControl(self.buttonR, 6, 7, columnspan=2)
		self.setFocus(self.buttonC)
		#
		self.connect(self.buttonL, self.close)
		self.connect(self.buttonC, lambda: xbmc.executebuiltin('RunAddon(script.itvviet.helper)'))
		self.connect(self.buttonR, lambda: self.__init__(not self.viet))

	def set_navigation(self):
		self.buttonL.controlRight(self.buttonC)
		self.buttonC.controlLeft(self.buttonL)
		self.buttonC.controlRight(self.buttonR)
		self.buttonR.controlLeft(self.buttonC)

####################################################################################################

def announce():
	CurrentAnnouncement = '20151019'
	LastAnnouncement = xbmc.getInfoLabel('Skin.String(Last.Announcement)')
	if CurrentAnnouncement <= LastAnnouncement:
		pass
	else:
		xbmc.executebuiltin('Skin.SetString(Last.Announcement,'+CurrentAnnouncement+')')
		models   = ['1d8c4ece2520039f5f0928ba0d8b8f3f01dfc426ec2db07e57fe36992a3d4a18dcdd13cce4659bddddd34410a530628752f69a006b58ff3215c7e4576f41dc75','7982c07f5eacf0b40b773ade397af5d86875aa87e5b21c68269e5c5aca00b3f46ff0539bc5b4359a8b9381a34a770ae346f76a6a1f94ba19be3987b06108f71d','83db8a25715370079a0c12fc3ff2863e0a572e5457298ce5b300aec32fb43704bbdec6247be051a27aeb637aa57541fd23b4ce6b47c5a950930832120d0d7575','6fc4b9e7970d2570786a47aa024c250a700947e347a05cfec46587fca6301199cbe5d6fc4316e411b8c0b007682256d230c01c5e15ab953fc2aa40d2e5dd338b']
		testModel = hashlib.sha512(miniIntel()).hexdigest()
		if (testModel in models):
			a = Announcement(True)
			a.doModal()
			del a

####################################################################################################