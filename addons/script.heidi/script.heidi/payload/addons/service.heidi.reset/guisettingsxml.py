import re
from xml.dom.minidom import parse

class GuiSettingsXml:
    """
    This class handles all interaction with XBMC's GuiSettings.xml file.
    """
    def __init__(self, guisettingsXml):
        self.guisettingsXml = guisettingsXml

    def setRSS(self):
        dom = parse(self.guisettingsXml)
        enablerssfeeds = "<enablerssfeeds>true</enablerssfeeds>"
        guisettingsXml = self.readGuiSettingXml()
        result = re.sub(r'<enablerssfeeds.*</enablerssfeeds>', enablerssfeeds, guisettingsXml)
        self.writeGuiSettingXml(result)

    def setWeatherAddon(self):
        dom = parse(self.guisettingsXml)
        addon = "<addon>weather.yahoo</addon>"
        guisettingsXml = self.readGuiSettingXml()
        result = re.sub(r'<addon.*</addon>', addon, guisettingsXml)
        self.writeGuiSettingXml(result)

    def readGuiSettingXml(self):
        fh = file(self.guisettingsXml, 'r')
        guisettingsXml = fh.read()
        fh.close()
        return guisettingsXml

    def writeGuiSettingXml(self, contents):
        fh = file(self.guisettingsXml, 'w')
        fh.write(contents)
        fh.close()
