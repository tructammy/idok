import os, xbmc, xbmcgui
import profilesxml, guisettingsxml
import xml.etree.ElementTree as ET

SLEEP_TIME = 1000

class Reset:

	profiles = profilesxml.ProfilesXml(xbmc.translatePath('special://userdata/profiles.xml'))
	guisettings = guisettingsxml.GuiSettingsXml(xbmc.translatePath('special://userdata/guisettings.xml'))

	def runService(self):

	# run until XBMC quits
		while(not xbmc.abortRequested):
			if not xbmc.abortRequested:
				xbmc.sleep(SLEEP_TIME)

		profiles_tree = ET.parse(xbmc.translatePath('special://userdata/profiles.xml'))
		profiles_root = profiles_tree.getroot()

		guisettings_tree = ET.parse(xbmc.translatePath('special://userdata/guisettings.xml'))
		guisettings_root = guisettings_tree.getroot()

	# set default lock
		tag = profiles_root.find('profile')
		status = tag.find('lockmode').text
		if status == '0':
			self.profiles.setLockMode()
			self.profiles.setLockCode()

	# set rss on default
		tag = guisettings_root.find('lookandfeel')
		status = tag.find('enablerssfeeds').text
		if status == 'false':
			self.guisettings.setRSS()

	# set default weather
		tag = guisettings_root.find('weather')
		status = tag.find('addon').text
		if status is not 'weather.yahoo':
			self.guisettings.setWeatherAddon()