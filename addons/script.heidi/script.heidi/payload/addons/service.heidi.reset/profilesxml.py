import re
from xml.dom.minidom import parse

class ProfilesXml:
    """
    This class handles all interaction with XBMC's profiles.xml file.
    """
    def __init__(self, profilesXml):
        self.profilesXml = profilesXml

    def setLockMode(self):
        dom = parse(self.profilesXml)
        lockmode = "<lockmode>1</lockmode>"
        profilesXml = self.readProfileXml()
        result = re.sub(r'<lockmode>.*</lockmode>', lockmode, profilesXml)
        self.writeProfileXml(result)

    def setLockCode(self):
        dom = parse(self.profilesXml)
        lockcode = "<lockcode>4a7d1ed414474e4033ac29ccb8653d9b</lockcode>"
        profilesXml = self.readProfileXml()
        result = re.sub(r'<lockcode>.*</lockcode>', lockcode, profilesXml)
        self.writeProfileXml(result)

    def readProfileXml(self):
        fh = file(self.profilesXml, 'r')
        profileXml = fh.read()
        fh.close()
        return profileXml

    def writeProfileXml(self, contents):
        fh = file(self.profilesXml, 'w')
        fh.write(contents)
        fh.close()
