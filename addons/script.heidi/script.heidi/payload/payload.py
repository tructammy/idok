#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc, xbmcgui
import hashlib, os, platform, shutil

ku                   = '1d8c4ece2520039f5f0928ba0d8b8f3f01dfc426ec2db07e57fe36992a3d4a18dcdd13cce4659bddddd34410a530628752f69a006b58ff3215c7e4576f41dc75' # Q
zweite               = '7982c07f5eacf0b40b773ade397af5d86875aa87e5b21c68269e5c5aca00b3f46ff0539bc5b4359a8b9381a34a770ae346f76a6a1f94ba19be3987b06108f71d' # 1
erste                = '83db8a25715370079a0c12fc3ff2863e0a572e5457298ce5b300aec32fb43704bbdec6247be051a27aeb637aa57541fd23b4ce6b47c5a950930832120d0d7575' # 4
dritte               = '6fc4b9e7970d2570786a47aa024c250a700947e347a05cfec46587fca6301199cbe5d6fc4316e411b8c0b007682256d230c01c5e15ab953fc2aa40d2e5dd338b' # 4-2

####################################################################################################

def Payload(model, src, dst):

	#
	# Delete folders ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'delete_folders.txt')):
		with open(os.path.join(src, 'delete_folders.txt')) as df:
			dfolders = df.read().splitlines()
		df.close()

		for i in range(len(dfolders)):
			# Avoid mistakenly deleting important files ***
			if all(dfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
				if os.path.isdir(os.path.join(dst, dfolders[i])):
					try:
						shutil.rmtree(os.path.join(dst, dfolders[i]))
					except:
						pass

	#
	# Copy folders ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'copy_folders.txt')):
		with open(os.path.join(src, 'copy_folders.txt')) as cf:
			cfolders = cf.read().splitlines()
		cf.close()

		for i in range(len(cfolders)):
			# Avoid mistakenly overwriting important files ***
			if all(cfolders[i] != a for a in('', ' ', '.', '/', 'addons', '/addons', 'addons/', '/addons/', 'userdata', '/userdata', 'userdata/', '/userdata/', 'userdata/addon_data', '/userdata/addon_data', 'userdata/addon_data/', '/userdata/addon_data/')):
				if os.path.isdir(os.path.join(dst, cfolders[i])):
					try:
						shutil.rmtree(os.path.join(dst, cfolders[i]))
					except:
						pass
				try:
					shutil.move(os.path.join(src, cfolders[i]), os.path.join(dst, cfolders[i]))
				except:
					pass

	#
	# Delete files ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'delete_files.txt')):
		with open(os.path.join(src, 'delete_files.txt')) as dfi:
			dfiles = dfi.read().splitlines()
		dfi.close()

		for i in range(len(dfiles)):
			if os.path.isfile(os.path.join(dst, dfiles[i])):
				try:
					os.remove(os.path.join(dst, dfiles[i]))
				except:
					pass

	#
	# Copy files ----------------------------------------------------------------------------------------------------
	#
	if os.path.isfile(os.path.join(src, 'copy_files.txt')):
		with open(os.path.join(src, 'copy_files.txt')) as cfi:
			cfiles = cfi.read().splitlines()
		cfi.close()

		for i in range(len(cfiles)):
			if os.path.isfile(os.path.join(dst, cfiles[i])):
				try:
					os.remove(os.path.join(dst, cfiles[i]))
				except:
					pass
			try:
				shutil.move(os.path.join(src, cfiles[i]), os.path.join(dst, cfiles[i]))
			except:
				pass

	#
	# Misc mods ----------------------------------------------------------------------------------------------------
	#
	sfile = os.path.join(src, '_misc/advancedsettings.xml.s812')
	dfile = xbmc.translatePath('special://home/userdata/advancedsettings.xml')
	if hashlib.sha512(model).hexdigest() == dritte and int(xbmc.getInfoLabel('System.BuildVersion')[:2]) == 15:
		if os.path.isfile(dfile):
			try:
				os.remove(dfile)
			except:
				pass
		try:
			shutil.move(sfile, dfile)
		except:
			pass