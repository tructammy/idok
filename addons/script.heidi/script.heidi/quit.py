#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc, xbmcaddon, xbmcgui, xbmcplugin
import os, platform, subprocess

if xbmc.getCondVisibility('System.Platform.Android'):
	home                 = xbmc.translatePath('special://home/')
	platform_system      = platform.system().rstrip('\n')
	platform_release     = platform.release().rstrip('\n')
	platform_machine     = platform.machine().rstrip('\n')

	# Parse *NIX commands for compatibility
	if platform_release == '3.0.36+':
		cmd_ = 'exec '; _cmd = ''
	else:
		cmd_ = 'exec su -c "'; _cmd = '"'

	#Hack to get current xbmc app id
	xbmcfolder=xbmc.translatePath(home).split('/')
	found = False
	i = 0
	for folder in xbmcfolder:
		if folder.count('.') >= 2 :
			found = True
			break
		else:
			i+=1
	if found == True:
		app_id = xbmcfolder[i]

	prompt = xbmcgui.Dialog().yesno('QUIT NOW','Are you sure?')
	if prompt:
		subprocess.call([cmd_+'/system/bin/am force-stop '+app_id+_cmd], executable='/system/bin/sh', shell=True)