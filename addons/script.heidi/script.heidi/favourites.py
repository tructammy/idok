import xbmc, os, re

src = xbmc.translatePath('special://home/userdata/favourites.xml')
key = int(sys.argv[1])

if os.path.isfile(src):
	with open(src) as f:
		favourites = f.read()
	f.close()

	fav = re.findall(r'<favourite .*</favourite>', favourites)

	if key <= len(fav):
		cmd = re.sub(r'<(.|\n)+?>', '', fav[key-1])
		cmd = cmd.replace('&amp;',  '&')
		cmd = cmd.replace('&quot;', '"')

		xbmc.executebuiltin(cmd)