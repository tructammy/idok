import xbmc, xbmcgui, os, shutil, time
from datetime import datetime

xbmc_path = xbmc.translatePath('special://home/')

#Hack to get current xbmc app id
if xbmc.getCondVisibility('System.Platform.Android'):
	xbmcfolder=xbmc.translatePath(xbmc_path).split('/')
	found = False
	i = 0
	for folder in xbmcfolder:
		if folder.count('.') >= 2 :
			found = True
			break
		else:
			i+=1
	if found == True:
		app_id = xbmcfolder[i]
		xbmc_data_path = os.path.join('/data/data/', app_id, '/')
	sdcard = '/sdcard/'
else:
	xbmc_data_path = xbmc_path
	sdcard = xbmc_path

#Set paths
folders = [
	os.path.join(xbmc_path, 'addons/packages'),
	os.path.join(xbmc_path, 'userdata/Thumbnails'),
	os.path.join(xbmc_path, 'userdata/Database/CDDB'),
	os.path.join(xbmc_path, 'cache'),
	os.path.join(xbmc_path, 'temp'),
	os.path.join(xbmc_data_path, 'cache'),
	os.path.join(sdcard, 'Download/APKs'),
	os.path.join(sdcard, 'show_box'),
	os.path.join(sdcard, 'SingerKaraoke')
]
files = [
	os.path.join(xbmc_path, 'userdata/Database/Textures13.db'),
	os.path.join(xbmc_path, 'userdata/Database/Addons16.db'), # KODI 14
	os.path.join(xbmc_path, 'userdata/Database/Epg8.db'),
	os.path.join(xbmc_path, 'userdata/Database/MyMusic48.db'),
	os.path.join(xbmc_path, 'userdata/Database/MyVideos90.db'),
	os.path.join(xbmc_path, 'userdata/Database/TV26.db'), 
	os.path.join(xbmc_path, 'userdata/Database/Addons19.db'), # KODI 15
	os.path.join(xbmc_path, 'userdata/Database/Epg10.db'),
	os.path.join(xbmc_path, 'userdata/Database/MyMusic52.db'),
	os.path.join(xbmc_path, 'userdata/Database/MyVideos93.db'),
	os.path.join(xbmc_path, 'userdata/Database/TV29.db'),
	os.path.join(xbmc_path, 'userdata/Database/Addons20.db'), # KODI 16
	os.path.join(xbmc_path, 'userdata/Database/Epg11.db'),
	os.path.join(xbmc_path, 'userdata/Database/MyMusic56.db'),
	os.path.join(xbmc_path, 'userdata/Database/MyVideos99.db'),
	os.path.join(xbmc_path, 'userdata/Database/TV29.db'),
	os.path.join(xbmc_path, 'userdata/Database/onechannelcache.db'), # THIRD PARTY
	os.path.join(xbmc_path, 'userdata/Database/saltscache.db')
]

def Clear(folders, files):
#Folders
	for i in range(len(folders)):
		if os.path.isdir(folders[i]):
			try:
				shutil.rmtree(folders[i])
			except:
				pass
#Files
	for i in range(len(files)):
		if os.path.isfile(files[i]):
			try:
				os.remove(files[i])
			except:
				pass

def Run():
	pDialog = xbmcgui.DialogProgress(); pDialog.create('Clearing Cache', 'PLEASE WAIT...')
	xbmc.sleep(1000)
	Clear(folders,files) #FIRST PASS
	xbmc.executebuiltin('Skin.SetString(Last.ClearCache,' + str(time.mktime(datetime.now().timetuple())) + ')')
	xbmc.executebuiltin('Quit')
	Clear(folders,files) #SECOND

if __name__ == '__main__':
	try: # Reads last clear date
		LastClearCache = datetime.fromtimestamp(float(xbmc.getInfoLabel('Skin.String(Last.ClearCache)')))
	except:
		if os.path.isfile(os.path.join(xbmc_path, 'userdata/Database/Addons16.db')): # Gets date from addons database for KODI 14
			LastClearCache = min(os.path.getatime(os.path.join(xbmc_path, 'userdata/Database/Addons16.db')), os.path.getctime(os.path.join(xbmc_path, 'userdata/Database/Addons16.db')), os.path.getmtime(os.path.join(xbmc_path, 'userdata/Database/Addons16.db')))
		elif os.path.isfile(os.path.join(xbmc_path, 'userdata/Database/Addons19.db')): # Gets date from addons database for KODI 15
			LastClearCache = min(os.path.getatime(os.path.join(xbmc_path, 'userdata/Database/Addons19.db')), os.path.getctime(os.path.join(xbmc_path, 'userdata/Database/Addons19.db')), os.path.getmtime(os.path.join(xbmc_path, 'userdata/Database/Addons19.db')))
		else: # Sets current date
			LastClearCache = time.mktime(datetime.now().timetuple())
		xbmc.executebuiltin('Skin.SetString(Last.ClearCache,' + str(LastClearCache) + ')')
		LastClearCache = datetime.fromtimestamp(LastClearCache)
	finally: # Checks interval between dates
		cachedDays = (datetime.now() - LastClearCache).days
	#Prompts user
	proceed = xbmcgui.Dialog().yesno('Cache cleared ' + str(cachedDays) + ' day(s) ago', 'CLEAR CACHE NOW?')
	if proceed: Run()