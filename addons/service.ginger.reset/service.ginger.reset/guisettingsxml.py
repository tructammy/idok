import re
from xml.dom.minidom import parse

class GuiSettingsXml:
    """
    This class handles all interaction with XBMC's GuiSettings.xml file.
    """
    def __init__(self, guisettingsXml):
        self.guisettingsXml = guisettingsXml

    def setWeatherAddon(self):
        dom = parse(self.guisettingsXml)
        addon = "<addon>weather.yahoo</addon>"
        guisettingsXml = self.readGuiSettingXml()
        result = re.sub(r'<addon .*</addon>', addon, guisettingsXml)
        self.writeGuiSettingXml(result)

    def readGuiSettingXml(self):
        fh = file(self.guisettingsXml, 'r')
        guisettingsXml = fh.read()
        fh.close()
        return guisettingsXml

    def writeGuiSettingXml(self, contents):
        fh = file(self.guisettingsXml, 'w')
        fh.write(contents)
        fh.close()
