import os, xbmc, xbmcgui
import profilesxml, guisettingsxml

SLEEP_TIME = 1000

class Reset:
    
    profiles = profilesxml.ProfilesXml(xbmc.translatePath('special://userdata/profiles.xml'))
    guisettings = guisettingsxml.GuiSettingsXml(xbmc.translatePath('special://userdata/guisettings.xml'))

    def runService(self):

    # run until XBMC quits
        while(not xbmc.abortRequested):
            if not xbmc.abortRequested:
                xbmc.sleep(SLEEP_TIME)

    # set default lock
        import xml.etree.ElementTree as ET
        tree = ET.parse(xbmc.translatePath('special://userdata/profiles.xml'))
        root = tree.getroot()
        tag = root.find('profile')
        status = tag.find('lockmode').text
        
        if status == '0':
            self.profiles.setLockMode()
            self.profiles.setLockCode()

    # set default weather
    #    import xml.etree.ElementTree as ET2
    #    tree2 = ET2.parse(xbmc.translatePath('special://userdata/guisettings.xml'))
    #    root2 = tree2.getroot()
    #    tag2 = root2.find('weather')
    #    status2 = tag2.find('addon').text
    #    
    #    if status2 == '':
    #        self.guisettings.setWeatherAddon()